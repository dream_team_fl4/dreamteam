var PlayerManager = new PlayerManager();
var ViewManager = new ViewManager();
var CardManager = new CardManager(6);
var DragAndDrop = new DragAndDrop();
var StorageManager = new StorageManager;

PlayerManager.addPlayer({name: "Player 1", gender: "male", isHost:true, lvl:1});
PlayerManager.addPlayer({name: "Player 2", gender: "male", isHost:false, lvl:1});
PlayerManager.addPlayer({name: "Player 3", gender: "male", isHost:false, lvl:1});
PlayerManager.addPlayer({name: "Player 4", gender: "male", isHost:false, lvl:1});
PlayerManager.addPlayer({name: "Player 5", gender: "male", isHost:false, lvl:1});
PlayerManager.addPlayer({name: "Player 6", gender: "male", isHost:false, lvl:1});

var GameManager = new GameManager();
var GameController = new GameController();

var ConnectionManager = new ConnectionManager(GameManager);
var Chat = new Chat(ConnectionManager, PlayerManager);
var RemoteGameController = new RemoteGameController(ConnectionManager, CardManager, GameManager,
    PlayerManager, ViewManager, Chat, GameController);

GameController.startGame();