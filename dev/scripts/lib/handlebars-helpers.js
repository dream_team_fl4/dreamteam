Handlebars.registerHelper('cif', function(item, options) {
    if (item == "door") {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

Handlebars.registerHelper('hasID', function(item, options) {
    if (item !== undefined) {
        return options.fn(this);
    }
});