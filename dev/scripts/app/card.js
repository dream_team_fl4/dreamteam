function Card(obj) {
  _extend(this, obj);

  function _extend(dist, src) {
    for (var prop in src) {
      if (src.hasOwnProperty(prop)) {
        dist[prop] = src[prop];
      }
    }
  };
}
