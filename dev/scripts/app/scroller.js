function initScrolls() {
    $(".handSlider").mCustomScrollbar({
		axis:"x",
		theme:"dark-thin",
        autoExpandScrollbar:true,
        autoHideScrollbar: true,
		advanced:{
            autoExpandHorizontalScroll:true,
            updateOnContentResize: true
        }
	});

    $(".itemsSlider").mCustomScrollbar({
		axis:"x",
		theme:"dark-thin",
        autoExpandScrollbar:true,
        autoHideScrollbar: true,
		advanced:{
            autoExpandHorizontalScroll:true,
            updateOnContentResize: true
        }
	});
}

function updateScrolls() {
    $(".handSlider").mCustomScrollbar("update");
    $(".itemsSlider").mCustomScrollbar("update");
}