function GameController() {

    /**
     * Starts a game, gives 8 cards for each player and determinate player's order.
     */

    function startGame() {
        CardManager.init().then(function () {
            ViewManager.loadAll().then(function () {
                RemoteGameController.init().then(function () {
                    // send initial decks
                    PlayerManager.setHeroCards();

                    var playersCount = PlayerManager.getPlayersCount();
                    for (var i = 0; i < playersCount; i++) {
                        for (var j = 0; j < 4; j++) {
                            CardManager.givePlayerDoor(i);
                        }

                        CardManager.givePlayerTreasure(i, 4);
                    }

                    RemoteGameController.sendInitialCardsState();
                    RemoteGameController.sendInitialPlayersState();
                    RemoteGameController.sendTurnConfiguration();

                    var player = _getCurrentPlayer();

                    if (player.name && player.gender) {
                        RemoteGameController.sendJoinGame(player);
                    }
                    else {
                        RemoteGameController.sendJoinGame({ name: 'Please Login', gender: 'male' });
                    }

                    GameController.updateBattlePower();
                    initScrolls();
                });
            });
        });
    }

    function _getCurrentPlayer() {
        return {
            name: StorageManager.getPlayerName(),
            gender: StorageManager.getPlayerGender(),
            id: StorageManager.getPlayerId()
        };
    }

    /**
     * Render Table view according to game flow.
     */
    function updateTable() {
        if (!GameManager.isGameStarted()) return;

        ViewManager.cleanTableView();
        var playerCards = CardManager.getPlayerCards(GameManager.getMainPlayer());
        var playersCount = PlayerManager.getPlayersCount();
        var treasureRebound = CardManager.getTreasureRebound();
        var doorRebound = CardManager.getDoorRebound();
        var phase = GameManager.getPhase();

        for (var i = 0; i < playersCount; i++) {
            var currentPlayerInfo = PlayerManager.getPlayerInfo(i);
            if (i == GameManager.getMainPlayer()) {
                ViewManager.renderView({data: {info: currentPlayerInfo, index: i}}, "info", "mainPlayer");
            } else {
                ViewManager.renderView({data: {info: currentPlayerInfo, index: i}}, "other", "otherPlayers");
            }
        }

        ViewManager.renderView(playerCards.hand, "hand", "card");
        ViewManager.renderView(playerCards.deskEnabled, "items", "card");
        ViewManager.renderView(GameManager.getMonsterCards(), "mob", "card");
        ViewManager.renderView(GameManager.getPlayerCards(), "player", "card");
        ViewManager.renderView(
            {
                data: {
                    treasureName: treasureRebound.length - 1 == -1 ? undefined : treasureRebound[treasureRebound.length - 1].name,
                    doorName: doorRebound.length - 1 == -1 ? undefined : doorRebound[doorRebound.length - 1].name
                }
            },
            "discard", "discard");

        updateScrolls();
        /**
         * Makes cards in battle undragable
         */
        $("#mob").children().attr("class", "card");
        $("#player").children().attr("class", "card");
        if (phase == "combat" || phase == "start") {
            var ready = GameManager.syncReadyPlayers();
            ViewManager.displayVoting(ready, GameManager.getMainPlayer());
            $(".isReady").show();
            $(".ready").show();
            if (GameManager.getMainPlayer() == GameManager.getTurn() && phase == "combat") {
                $(".runA").show();
                $(".help").show();
            } else {
                $(".runA").hide();
                $(".help").hide();
            }
        } else if (phase == "finish" && GameManager.getMainPlayer() == GameManager.getTurn()) {
            $(".ready").show();
            $(".runA").hide();
        } else {
            $(".isReady").hide();
            $(".ready").hide();
            $(".help").hide();
        }

        /**
         * display curse button if player affected by curse
         */
        $(".curse").hide();
        for (var card in playerCards.deskCurse) {
            if (hasOwnProperty.call(playerCards.deskCurse, card)) {
                $(".curse").show();
                break;
            }
        }
        if (GameManager.getPhase() != "" && GameManager.getPhase() != "start") {
            ViewManager.updateDisabled(GameManager.getTurn());
        }

        $(".curses").hide();
        for (var index=0; index<PlayerManager.getPlayersCount(); index++ ){
            var cards = CardManager.getPlayerCards(index);
            if (Object.keys(cards.deskCurse).length > 0){
                $("#player"+index).find(".curses").show();
            }
        }
    }

    /**
     * Update Battle Power according to new data
     */
    function updateBattlePower() {
        var isHost = PlayerManager.isHost(GameManager.getMainPlayer());
        var renderData = {
            power: GameManager.getMonsterPower(),
            isHost: isHost
        }
        ViewManager.cleanView("inputPower");
        ViewManager.renderView({data: renderData}, "inputPower", "monsterPower");

        renderData.power = GameManager.getPlayerPower();
        ViewManager.renderView({data: renderData}, "inputPower", "playerPower");

        if (GameManager.getMainPlayer() == GameManager.getTurn()) {
            $(".runA").show();
        } else {
            $(".runA").hide();
        }
    }

    /**
     * Process card drop
     * @return {boolean} true if drob successfully
     */
    function dragEnd(dragObject, dropZone) {
        var isSuccess = true;
        /**
         * playerCards - contains object of all player's cards
         */
        var playerCards = CardManager.getPlayerCards(GameManager.getMainPlayer());
        var dragItem = findCard(playerCards, dragObject.cardName);
        /**
         * Scroller overbuild div#items, so drag'n'drop haven't access to this block and dropZone.attr("id")
         * is undefined. That's why if dropZone.attr("id") == false we define "items" as dropzone
         */
        var isActionForbidden = GameController.isActionForbidden(dropZone.attr("id") || "items", dragItem);
        if (isActionForbidden) {
            return false;
        }
        /* Equip item case */
        if (dropZone.attr("class").indexOf("table_cards") != -1) {
            /**
             * slotStatus contains info about slot status
             * allow - answer question "Do I can equip this, allowed only for some Class, card?"
             * itemsToSwap - array of items in busy slot
             */
            var slotStatus = CardProcessor.checkSlot(playerCards.deskEnabled, dragItem);
            /**
             * if array of items in busy slot is empty and item passed class check
             */
            if (slotStatus.itemsToSwap.length == 0 && slotStatus.allow) {
                CardManager.putEnabledCardOnPlayerDesk(GameManager.getMainPlayer(), dragItem.name);
            } else {
                if (!slotStatus.allow) {
                    ViewManager.callDialog("Warning!", "You can't equip this item because of item class.");
                    isSuccess = false;
                } else {
                    ViewManager.confirmInformation("Change Item", {
                        msg: "WARNING! You already have " + dragItem.cardClass + " item.\nDo you want to discard equiped item and equip new one?",
                        ok: "okChangeItem",
                        cancel: "cancelChangeItem"
                    });
                }
                $("#okChangeItem").off().on("click", function() {
                    setTimeout(function() {
                        if ((dragItem.cardClass == "one hand" || dragItem.cardClass == "class" ) && slotStatus.itemsToSwap[1]) {
                            ViewManager.chooseEquipment({
                                title: "Warning!",
                                msg: "You already have "+ dragItem.cardClass +" equiped. Choose card which one you want ot discard.",
                                ok: "okSaveItem",
                                cancel: "cancelSaveItem",
                                cardToEquip: dragItem.name
                            });
                            ViewManager.renderView(slotStatus.itemsToSwap, "equipment", "cardBig");
                        } else {
                            slotStatus.itemsToSwap.forEach(function (item) {
                                CardManager.putPlayerEnabledDeskCardToRebound(GameManager.getMainPlayer(), item.name);
                            });
                            CardManager.putEnabledCardOnPlayerDesk(GameManager.getMainPlayer(), dragItem.name);
                            GameController.refreshPower(GameManager.getMainPlayer());
                            RemoteGameController.sendSyncCards();
                            GameController.updateTable();
                        }
                    }, 200);

                    $(document).on("click", "#okSaveItem", function() {
                        var cardToEquip = $(".chooseEquipment").attr("data-equip");
                        var cardName = $("#equipment").find(".selectgiveCard").attr("data-name");
                        CardManager.putPlayerEnabledDeskCardToRebound(GameManager.getMainPlayer(), cardName);
                        CardManager.putEnabledCardOnPlayerDesk(GameManager.getMainPlayer(), cardToEquip);
                        GameController.refreshPower(GameManager.getMainPlayer());
                        RemoteGameController.sendSyncCards();
                        GameController.updateTable();
                        $('.chooseEquipment').remove();
                    });

                    $(document).on("click", "#cancelSaveItem", function() {
                        $('.chooseEquipment').remove();
                    });
                });
            }

        /* Assist monster case */
        } else if (dropZone.attr("id") == "mob") {
            monsterCardsStatus = CardProcessor.checkMonsterCards(GameManager.getMonsterCards(), dragItem);
            if (monsterCardsStatus.allow && monsterCardsStatus.allow != "WM") {
                monsterCardsStatus.cardsToPlay.forEach(function (card, index) {
                    GameManager.addMonsterCard(card);
                    CardManager.playHandCard(GameManager.getMainPlayer(), card.name);
                });
            } else if(monsterCardsStatus.allow != "WM") {
                ViewManager.callDialog("Warning!", "You can't play this card.");
                isSuccess = false;
            }

        /* Assist player case */
        } else if (dropZone.attr("id") == "player") {
            GameManager.addPlayerCard(dragItem);
            CardManager.playHandCard(GameManager.getMainPlayer(), dragItem.name);

        /* Discard case */
    } else if (dropZone.attr("id") == "discard") {
            ViewManager.confirmInformation("Look!", {
                msg: "Do You want to discard " + dragItem.name + "?",
                ok: "okDiscard",
                cancel: "cancelDiscard"
            });
            $("#okDiscard").off().on("click", function() {
                setTimeout(function() {
                    if (dragItem.cancel) {
                        CardProcessor.processCancel(playerCards.deskCurse, dragItem);
                    } else {
                        /**
                         * discard special items
                         */
                        if (dragItem.name == "universal_translator_device" || dragItem.name == "royalty" || dragItem.name == "supermunchkin") {
                            CardProcessor.cleanEquipedItems(playerCards.deskEnabled, dragItem.name);
                            // unequip.forEach(function (item) {
                            //     CardManager.putPlayerCardToRebound(GameManager.getMainPlayer(), item);
                            // });
                        } else {
                            CardManager.putPlayerCardToRebound(GameManager.getMainPlayer(), dragItem.name);
                        }
                    }
                    RemoteGameController.sendSyncCards();
                    GameController.updateTable();
                }, 200)
            });
        /* Effect another player */
        } else if (dropZone.attr("class").indexOf("character") != -1) {
            var playerIndex = $(dropZone).attr("data-index");
            if (dragItem.cardClass == "curse") {
                isSuccess = CardProcessor.applyCurse(dragItem, +playerIndex);
            } else if (dragItem.cardClass == "lvlUp") {
                isSuccess = CardProcessor.processLvlUp(dragItem, +playerIndex);
                if (isSuccess) {
                    if (dragItem.name == "patry_with_party_pat") {
                        var isMusician = false;
                        for (var key in playerCards.deskEnabled) {
                            if (key == "musician") {
                                CardProcessor.processCancel(playerCards.deskCurse, dragItem);
                                isMusician = true;
                                break;
                            }
                        }
                        if (!isMusician) {
                            CardManager.putPlayerCardToRebound(GameManager.getMainPlayer(), dragItem.name);
                        }
                    } else {
                        CardManager.putPlayerCardToRebound(GameManager.getMainPlayer(), dragItem.name);
                    }
                }
                GameController.refreshPower(+playerIndex);
            }
            /* Effect himself */
        } else if (dropZone.attr("id") == "info") {
            isSuccess = CardProcessor.processLvlUp(dragItem, GameManager.getMainPlayer());
            if (isSuccess) {
                if (dragItem.name == "patry_with_party_pat") {
                    var isMusician = false;
                    for (var key in playerCards.deskEnabled) {
                        if (key == "musician") {
                            CardProcessor.processCancel(playerCards.deskCurse, dragItem);
                            isMusician = true;
                            break;
                        }
                    }
                    if (!isMusician) {
                        CardManager.putPlayerCardToRebound(GameManager.getMainPlayer(), dragItem.name);
                    }
                } else {
                    CardManager.putPlayerCardToRebound(GameManager.getMainPlayer(), dragItem.name);
                }

            }
        }
        /**
         * sync cards if card is dropped
         */
        if (isSuccess) {
            GameController.refreshPower(GameManager.getMainPlayer());
            RemoteGameController.sendSyncCards();
        }

        return isSuccess;
    }

    function isActionForbidden(dropZone, dragItem) {
        var phase = GameManager.getPhase();
        var turn = GameManager.getTurn();
        var sidekick = GameManager.getSidekick();
        var actionPlayerID = GameManager.getMainPlayer();

        if (phase == "start" && (dropZone == "player" || dropZone == "mob")) {
            ViewManager.callDialog("Warning!", "Wait for game start.");
            return true;
        }

        if (phase == "finish" && (dropZone == "player" || dropZone == "mob")) {
            ViewManager.callDialog("Warning!", "Wait for next turn.");
            return true;
        }

        if (phase == "combat") {
            if (dropZone == "items") {
                ViewManager.callDialog("Warning!", "You can't equip now!");
                return true;
            } else if (!(actionPlayerID == sidekick.id || actionPlayerID == turn) && dropZone == "player") {
                ViewManager.callDialog("Warning!", "You can't assist now! Player must ask for help.");
                return true;
            }
        }

        return false;
    }

    /**
     * start and proccedure phase
     */
    function phase(name) {
        /**
         * sync cards if any phase started
         */
        RemoteGameController.sendSyncCards();
        RemoteGameController.sendPhaseChange(name);
        var actionPlayerID = GameManager.getMainPlayer();
        var turnPlayerID = GameManager.getTurn();
        var playerCards = CardManager.getPlayerCards(turnPlayerID);
        switch (name) {
            case "start":
                GameManager.startPhase("start");
                GameController.updateTable();
                //_sendMessage("Game Started. Equip Items and press Ready button");
                break;
            case "kickTheDoor":
                GameManager.startPhase("kickTheDoor");
                if (actionPlayerID == turnPlayerID) {
                    var door = CardManager.takeCardFromDoorDeckAndPlay(turnPlayerID);
                    GameManager.addMonsterCard(door);
                    ViewManager.cleanView("mob");
                    ViewManager.renderView(GameManager.getMonsterCards(), "mob", "card");
                    if (door.cardClass == "curse") {
                        CardProcessor.applyCurse(door, "self");
                    }
                    if (door.cardClass == "monster") {
                        GameController.phase("combat");
                    } else {
                        GameController.phase("lfTroubles");
                    }
                }
                break;
            case "combat":
                GameManager.startPhase("combat");
                for (var key in playerCards.deskCurse) {
                    if (playerCards.deskCurse[key].activate && playerCards.deskCurse[key].activate == "combat") {
                        GameManager.addPlayerCard(playerCards.deskCurse[key]);
                        CardManager.playCurseOnDesk(turnPlayerID, playerCards.deskCurse[key].name);
                        GameController.updateTable();
                        RemoteGameController.sendSyncCards();
                    }
                }
                GameController.updateTable();
                break;
            case "postCombat":
                GameManager.startPhase("postCombat");
                var monsterPower = GameManager.getMonsterPower();
                var playerPower = GameManager.getPlayerPower();
                if (monsterPower < playerPower) {
                    GameController.phase("win");
                } else {
                    if (actionPlayerID == turnPlayerID) {
                        GameController.runAway();
                    }
                    GameController.phase("finish");
                }
                break;
            case "win":
                GameManager.startPhase("win");
                if (actionPlayerID == turnPlayerID) {
                    var reward = GameManager.calculateReward();
                    var sidekick = GameManager.getSidekick();
                    if (sidekick.id != undefined) {
                        var sidekickCards = CardManager.getPlayerCards(sidekick.id);
                        for (var key in sidekickCards.deskEnabled) {
                            if (key == "hero") {
                                CardManager.givePlayerTreasure(sidekick.id, 1);
                            }
                        }
                        CardManager.givePlayerTreasure(sidekick.id, sidekick.reward);
                        reward.treasures -= sidekick.reward;
                    }
                    PlayerManager.raiseLevel(reward.lvl, turnPlayerID);
                    CardManager.givePlayerTreasure(turnPlayerID, reward.treasures);
                    GameController.phase("finish");
                }
                break;
            case "lfTroubles":
                GameManager.startPhase("lfTroubles");
                setTimeout(function() {
                    GameManager.clearTable();
                    GameController.updateTable();
                    if (GameManager.getMainPlayer() == GameManager.getTurn()) {
                        var monsters = [];
                        for (var key in playerCards.hand) {
                            if (playerCards.hand[key].cardClass == "monster") {
                                monsters.push(playerCards.hand[key]);
                            }
                        }
                        ViewManager.lfTroubleChoose({
                            monsters: monsters
                        });
                    }
                }, 10000);
                break;
            case "finish":
                GameManager.startPhase("finish");
                GameManager.clearTable();
                CardManager.putCardsToRebound();

                GameController.updateBattlePower();
                RemoteGameController.sendBattlePower();

                RemoteGameController.sendSyncCards();
                GameController.updateTable();
                GameController.refreshPower(GameManager.getMainPlayer());

                break;
            case "nextTurn":
                GameManager.startPhase("nextTurn");
                GameManager.endTurn();
                turnPlayerID = GameManager.getTurn();
                playerCards = CardManager.getPlayerCards(turnPlayerID);
                playerInfo = PlayerManager.getPlayerInfo(turnPlayerID);
                for (var key in playerCards.deskCurse) {
                    if (playerCards.deskCurse[key].activate && playerCards.deskCurse[key].activate == "turn") {
                        CardManager.putPlayerCurseDeskCardToRebound(turnPlayerID, playerCards.deskCurse[key].name);
                        GameController.updateTable();
                        RemoteGameController.sendLoseTurn();
                        GameManager.endTurn();
                        RemoteGameController.sendSystemMessage("Player " + playerInfo.name + " skips his turn.");
                    }
                }
                GameController.updateTable();
                GameController.phase("kickTheDoor");
                break;
        }
    }

    /**
     * sync phase with other players
     */
    function syncPhases(name) {
        var actionPlayerID = GameManager.getMainPlayer();
        var turnPlayerID = GameManager.getTurn();
        var playerCards = CardManager.getPlayerCards(actionPlayerID);
        switch (name) {
            case "kickTheDoor":
                GameManager.startPhase("kickTheDoor");
                if (actionPlayerID == turnPlayerID) {
                    var door = CardManager.takeCardFromDoorDeckAndPlay(turnPlayerID);
                    GameManager.addMonsterCard(door);
                    ViewManager.cleanView("mob");
                    ViewManager.renderView(GameManager.getMonsterCards(), "mob", "card");
                    if (door.cardClass == "curse") {
                        CardProcessor.applyCurse(door, "self");
                    }
                    if (door.cardClass == "monster") {
                        GameController.phase("combat");
                    } else {
                        GameController.phase("lfTroubles");
                    }
                }
                break;
            case "combat":
                GameManager.startPhase("combat");
                break;
            case "postCombat":
                GameManager.startPhase("postCombat");
                var monsterPower = GameManager.getMonsterPower();
                var playerPower = GameManager.getPlayerPower();
                if (monsterPower < playerPower) {
                } else {
                    if (actionPlayerID == turnPlayerID) {
                        GameController.runAway();
                    }
                }
                break;
            case "win":
                GameManager.startPhase("win");
                if (actionPlayerID == turnPlayerID) {
                    var reward = GameManager.calculateReward();
                    var sidekick = GameManager.getSidekick();
                    if (sidekick.id != undefined) {
                        var sidekickCards = CardManager.getPlayerCards(sidekick.id);
                        for (var key in sidekickCards.deskEnabled) {
                            if (key == "hero") {
                                CardManager.givePlayerTreasure(sidekick.id, 1);
                            }
                        }
                        CardManager.givePlayerTreasure(sidekick.id, sidekick.reward);
                        reward.treasures -= sidekick.reward;
                    }
                    PlayerManager.raiseLevel(reward.lvl, turnPlayerID);
                    CardManager.givePlayerTreasure(turnPlayerID, reward.treasures);
                    GameController.phase("finish");
                }
                break;
            case "lfTroubles":

                setTimeout(function() {
                    GameManager.clearTable();
                    GameController.updateTable();
                    if (GameManager.getMainPlayer() == GameManager.getTurn()) {
                        var monsters = [];
                        for (var key in playerCards.hand) {
                            if (playerCards.hand[key].cardClass == "monster") {
                                monsters.push(playerCards.hand[key]);
                            }
                        }
                        ViewManager.lfTroubleChoose({
                            monsters: monsters
                        });
                    }
                }, 10000);

                GameManager.startPhase("lfTroubles");
                break;
            case "finish":
                GameManager.startPhase("finish");
                GameManager.clearTable();
                break;
            case "nextTurn":
                GameManager.endTurn();
                GameManager.startPhase("nextTurn");
                break;
        }
        GameController.updateTable();
    }

    /**
     * manage player's hand cards amount
     * @return {boolean} is player has good cards amount
     */
    function checkHand() {
        var status = false;
        var actionPlayerID = GameManager.getMainPlayer();
        var cards = CardManager.getPlayerCards(actionPlayerID);
        var amount = 0;

        for (var name in cards.hand) {
            amount++;
        }

        if (amount <= 5) {
            status = true;
        } else {
            for (var name in cards.deskEnabled) {
                if (name == "backpack") {
                    status = true;
                }
            }
        }
        return status;
    }

    /**
     * returns equiped items power and name of most powerfull item
     * @param {number} - player's ID
     * @return {object} itemsInfo.name - name of most powerfull item, itemsInfo.power - current player power
     */
    function getEquipedItemsPower(playerID) {
        var itemsInfo = {};
        var topPower = 0;
        var itemPower = 0;
        var cards = CardManager.getPlayerCards(playerID);
        var playerInfo = PlayerManager.getPlayerInfo(playerID);
        var classes = [];
        itemsInfo.power = 0;
        for (var card in cards.deskEnabled) {
            if (cards.deskEnabled[card].cardClass == "class") {
                classes.push(cards.deskEnabled[card].name);
            }
        }
        for (var card in cards.deskEnabled) {
            if (cards.deskEnabled[card].power) {
                if (cards.deskEnabled[card].name == "axe_bass" && classes.indexOf("musician") != -1) {
                    itemPower = 4;
                } else if(cards.deskEnabled[card].name == "magic_shoe" && playerInfo.gender == "female") {
                    itemPower = 4;
                } else if(cards.deskEnabled[card].name == "magic_wand" && classes.indexOf("wizard") != -1) {
                    itemPower = 3;
                } else if (cards.deskEnabled[card].name == "nothung" && classes.indexOf("hero") != -1) {
                    itemPower = 4;
                } else if (cards.deskEnabled[card].name == "banana_guard" && classes.indexOf("royalty") != -1) {
                    itemPower = 4;
                } else if (cards.deskEnabled[card].name == "shelly" && classes.indexOf("musician") != -1) {
                    itemPower = 3;
                } else if (cards.deskEnabled[card].cardClass == "ally" && classes.indexOf("royalty") != -1) {
                    itemPower = cards.deskEnabled[card].power + 1;
                } else {
                    itemPower = cards.deskEnabled[card].power;
                }

                if (topPower < itemPower && cards.deskEnabled[card].cardClass != "itemBuff"  && cards.deskEnabled[card].cardClass != "ally") {
                    topPower = itemPower;
                    itemsInfo.name = cards.deskEnabled[card].name;
                }

                itemsInfo.power += itemPower;
            }
        }
        return itemsInfo;
    }

    /**
     * refresh player power by index
     */
    function refreshPower(index) {
        var itemsInfo = GameController.getEquipedItemsPower(index);
        var playerInfo = PlayerManager.getPlayerInfo(index);
        var totalPower = itemsInfo.power + playerInfo.lvl;
        if (itemsInfo.power != undefined) {
            PlayerManager.setPlayerPower(totalPower, index)
        }
        RemoteGameController.sendSyncPlayers();
    }
    /**
     * Run Away action
     */
    function runAway() {
        var roll;
        var tableCards = GameManager.getMonsterCards();
        var playerCards = CardManager.getPlayerCards(GameManager.getTurn());
        var playerInfo = PlayerManager.getPlayerInfo(GameManager.getTurn());
        var sidekick = GameManager.getSidekick();
        var monsters = [];
        var RA = 0;
        if (sidekick.id != undefined) {
            var sidekickInfo = PlayerManager.getPlayerInfo(sidekick.id);
        }
        RemoteGameController.sendSystemMessage("Hahaha! " + playerInfo.name + " runs away like a coward!");
        for (var card in playerCards.deskEnabled) {
            if (playerCards.deskEnabled[card].RA) {
                RA += playerCards.deskEnabled[card].RA;
            }
        }
        tableCards.forEach(function(card) {
            if (card.cardClass == "monster") {
                monsters.push(card);
            }
            if (card.RA) {
                RA += card.RA;
            }
        });
        monsters.forEach(function(monster) {
            roll = GameManager.roll();
            if (roll + RA>= 5) {
                RemoteGameController.sendSystemMessage("Roll:" + roll + " RA bonus:" + RA + ". " + playerInfo.name + " run successfully from " + monster.name);
            } else {
                if (monster.name != "crystal_guardian" && monster.name != "demon_cat" && monster.name != "door_lord") {
                    RemoteGameController.sendSystemMessage("Roll:" + roll + " RA bonus:" + RA + ". " + playerInfo.name + " suffer bad stuff from " + monster.name);
                }
                CardProcessor.applyBadStuff(monster, GameManager.getTurn())
            }
        });
        if (sidekick.id) {
            RemoteGameController.sendSystemMessage(sidekickInfo.name + " try to run away too.");
            monsters.forEach(function(monster) {
                roll = GameManager.roll();
                if (roll + RA>= 5) {
                    RemoteGameController.sendSystemMessage("Roll:" + roll + " RA bonus:" + RA + ". " + sidekickInfo.name + " run successfully from " + monster.name);
                } else {
                    RemoteGameController.sendSystemMessage("Roll:" + roll + " RA bonus:" + RA + ". " + sidekickInfo.name + " suffer bad stuff from " + monster.name);
                    CardProcessor.applyBadStuff(monster, sidekick.id)
                }
            });
        }
    }

    function lootTheBody(id, sidekick) {
        var playersCount = PlayerManager.getPlayersCount();
        var playerCards = CardManager.getPlayerCards(id);
        var players = [];
        var cardsToLose = [];
        for (var key in playerCards.deskEnabled) {
            if (playerCards.deskEnabled[key].cardClass != "class") {
                cardsToLose.push(key);
            }
        }
        for (var key in playerCards.hand) {
            cardsToLose.push(key);
            CardManager.putEnabledCardOnPlayerDesk(id, key);
        }
        for (var i = 0; i < playersCount; i++) {
            if (i == id) {
                continue;
            }
            players.push({player: PlayerManager.getPlayerInfo(i), id: i});
        }
        players.sort(function(a, b) {
            if (a.player.lvl < b.player.lvl)
                return 1;
            else if (a.player.lvl > b.player.lvl)
                return -1;
            else
                return 0;
        });
        var randomOrder = getRandomArray(0, cardsToLose.length)
        players.forEach(function(player, index) {
            if (cardsToLose[index]) {
                CardManager.giveOwnCardToOtherPlayer(id, player.id, cardsToLose.splice(randomOrder[index], 1)[0]);
            }
        });
        cardsToLose.forEach(function(card) {
            CardManager.putPlayerCardToRebound(id, card);
        });
        CardManager.givePlayerDoor(id);
        CardManager.givePlayerTreasure(id);
        GameController.updateTable();
        RemoteGameController.sendSyncCards();
    }

    function _sendMessage(message) {
        RemoteGameController.sendSystemMessage(message);
    }

    return {
        startGame: startGame,
        updateTable: updateTable,
        updateBattlePower: updateBattlePower,
        dragEnd: dragEnd,
        isActionForbidden: isActionForbidden,
        phase: phase,
        syncPhases: syncPhases,
        checkHand: checkHand,
        getEquipedItemsPower: getEquipedItemsPower,
        refreshPower: refreshPower,
        runAway: runAway,
        lootTheBody: lootTheBody
    }
}

/* == Host Set Battle Power BEGIN == */

$(document).on("click", "#oldMobPower", function () {
    $("#oldMobPower").hide();
    $("#changeMobPower").show();
    $("#setMobPower").show();
});

$(document).on("click", "#setMobPower", function () {
    var updatePowerValue = $("#newMobPowerVal").val();
    if (isNaN(updatePowerValue) || updatePowerValue < 1 || updatePowerValue > 150){
        ViewManager.callDialog("Warning!", "Not valid data, please write only numbers!");
    } else {
        GameManager.setMonsterPower(parseInt(updatePowerValue));
        GameController.updateBattlePower();
        RemoteGameController.sendBattlePower();
    }
});

$(document).on("click", "#oldPlayerPower", function () {
    $("#oldPlayerPower").hide();
    $("#changePlayerPower").show();
    $("#setPlayerPower").show();
});

$(document).on("click", "#setPlayerPower", function () {
    var updatePowerValue = $("#newPlayerPowerVal").val();
    if (isNaN(updatePowerValue) || updatePowerValue < 1 || updatePowerValue > 150){
       ViewManager.callDialog("Warning!", "Not valid data, please write only numbers!");
    }else {
        GameManager.setPlayerPower(parseInt(updatePowerValue));
        GameController.updateBattlePower();
        RemoteGameController.sendBattlePower();
    }
});
/* == Host Set Battle Power END == */

/* == Modal windows == */

$(document).on("click", ".playerInfo", function () {
    var playerData = PlayerManager.getPlayerInfo(GameManager.getMainPlayer());
    ViewManager.renderView({data: playerData}, 'modalWindows', 'playerInfo');
    $('.windowHero').modal('show');
});


$(document).on("click", ".otherInfo", function () {
    var id = $(this).attr("data-index");
    var playerData = PlayerManager.getPlayerInfo(+id);
    ViewManager.renderView({data: playerData}, 'modalWindows', 'playerInfo');
    $('.windowHero').modal('show');
});


$(document).on("click", ".viewHandCards", function () {
    var playerCards = CardManager.getPlayerCards(GameManager.getMainPlayer());
    ViewManager.renderView({title: "Your hand cards"}, 'modalWindows', 'windowAllhandCards');
    ViewManager.renderView(playerCards.hand, 'allCards', 'cardBig');
    $('.allHandCards').modal('show');
});



$(document).on("click", ".viewTableCards", function () {
    var playerCards = CardManager.getPlayerCards(GameManager.getMainPlayer());
    ViewManager.renderView({title: "Your items"}, 'modalWindows', 'windowAllTableCards');
    ViewManager.renderView(playerCards.deskEnabled, 'allTableCards', 'cardBig');
    $('.allTableCards').modal('show');
});

/*== Sell cards  ==*/
$(document).on("click", ".sell", function () {
    if (GameManager.getPhase() == "combat") {
        return;
    }
    var playerCards = CardManager.getPlayerCards(GameManager.getMainPlayer());
    var arraySellCards = [];
    for (var key in playerCards) {
        for (var value in playerCards[key]) {
            if (playerCards[key][value]['worth']) {
                arraySellCards.push(playerCards[key][value]);
            }
        }
    }
    ViewManager.renderView({title: "Cards for sell"}, 'modalWindows', 'sell');
    ViewManager.renderView(arraySellCards, 'cardsForSell', 'sellCards');
    $('.cardsSell').modal('show');
});

$(document).on("click", ".checkSale", function () {
    var worth = $("#sumCoin").text();
    var add = $(this).attr("data-worth");
    if ($(this).prop("checked")) {
        $("#sumCoin").text(parseInt(worth) + parseInt(add));
    } else {
        $("#sumCoin").text(parseInt(worth) - parseInt(add));
    }
    worth = parseInt($("#sumCoin").text());
    if (worth < 1000) {
        $("#sellCards").prop('disabled', true);
    } else {
        $("#sellCards").prop('disabled', false);
    }
});

$(document).on("click", "#sellCards", function () {
    var cards = [];
    $(".checkSale:checked").each(function(i, input) {
        cards.push($(input).attr("data-name"));
    });
    var reward = Math.floor(parseInt( $("#sumCoin").text() ) / 1000);
    PlayerManager.raiseLevel(reward, GameManager.getMainPlayer());
    cards.forEach(function(card) {
        CardManager.putPlayerCardToRebound(GameManager.getMainPlayer(), card);
    });
    $('.cardsSell').modal('hide');
    GameController.refreshPower(GameManager.getMainPlayer());
    GameController.updateTable();
    RemoteGameController.sendSyncCards();
});

/*== Sell cards END==*/

$(document).on("click", ".curse", function () {
    var playerCards = CardManager.getPlayerCards(GameManager.getMainPlayer());
    ViewManager.renderView({data: {title: "Curses"}}, 'modalWindows', 'curseWindow');
    ViewManager.renderView(playerCards.deskCurse, 'playerCurses', 'cardBig');
    $('#myModal1').modal('show');
});

$(document).on("click", ".curses", function () {
    var index = $(this).parent().attr("data-index");
    var cards = CardManager.getPlayerCards(index);
    ViewManager.renderView({data: {title: "Curses"}}, 'modalWindows', 'curseWindow');
    ViewManager.renderView(cards.deskCurse, 'playerCurses', 'cardBig');
    $('#myModal1').modal('show');
});


$(document).on("click", ".bags", function () {
    var index = $(this).parent().attr("data-index");
    var cards = CardManager.getPlayerCards(index);
    var allCards = {};
    for (var key in cards.deskEnabled) {
        allCards[key] = cards.deskEnabled[key];
    }
    ViewManager.renderView({data: {title: "items"}}, 'modalWindows', 'bags');
    ViewManager.renderView(allCards, 'cardsBag', 'cardBig');
    $('.bagsCard').modal('show');
});



/*== Trade ==*/

$(document).on("click", ".trade", function () {
    if (GameManager.getPhase() == "combat") {
        return;
    }
    var playerInfo = [];
    for (var i = 0; i < PlayerManager.getPlayersCount(); i++){
        if (i !== GameManager.getMainPlayer()){
            playerInfo.push({info:PlayerManager.getPlayerInfo(i), id:i});
        }
    }
    var playerCards = CardManager.getPlayerCards(GameManager.getMainPlayer());
    var allCards = {};
    for (var key in playerCards.deskEnabled) {
        if (playerCards.deskEnabled[key]['cardClass'] !== 'class'){
        allCards[key] = playerCards.deskEnabled[key];
    }
}
    var name = PlayerManager.getPlayerInfo(GameManager.getMainPlayer()).name;
    ViewManager.renderView({title: "Cards for trade"}, 'modalWindows', 'tradeCards');
    ViewManager.renderView(playerInfo, 'playersDropDown', 'dropDown');
    ViewManager.renderView(allCards, 'cardsTradeMain', 'cardsForTrade');
    $('.cardsTrade').modal('show');
    $('.mainPlayerName').text(name);
    $('#cardsTradeMain').modal('show');
});

$(document).on("click", ".player", function () {
    var selectPlayer = $(this).prop('class', 'select');
    var name = $(this).text();
    $('.PlayerName').text(name);
    var index = $(this).attr("data-id");
    var cards = CardManager.getPlayerCards(parseInt(index));
    var allCards = {};
    for (var key in cards.deskEnabled) {
        if (cards.deskEnabled[key]['cardClass'] !== 'class'){
        allCards[key] = cards.deskEnabled[key];
    }
}
    ViewManager.renderView(allCards, 'cardsTradePlayer', 'cardsForTrade');
    $('#cardsTradePlayer').modal('show');

});

$(document).on("click", ".cardTrade", function () {
    $(this).toggleClass("selectgiveCard");
});

$(document).on("click", "#TradeCards", function () {
    var index = $('.select').attr("data-id");
    var mainPlayerID = GameManager.getMainPlayer();
    var arrayElements = $("#cardsTradeMain").children(".selectgiveCard");
    var giveCards = [];
    $.each(arrayElements, function() {
        giveCards.push({name: $(this).attr("data-name"), deckType: $(this).attr("data-deckType")});
    });
    var arrayElements2 = $("#cardsTradePlayer").children(".selectgiveCard");
    var getCards = [];
    $.each(arrayElements2, function() {
        getCards.push({name: $(this).attr("data-name"), deckType: $(this).attr("data-deckType")});
    });
    RemoteGameController.sendTrade({give: giveCards, get: getCards, idFrom  : mainPlayerID, idTo: index, type: "ask"});
    $('.cardsTrade').modal('hide');
});

$(document).on("click", "#confirmTrade", function () {
    var index = parseInt($('#get').attr("data-id"));
    var arrayElements = $("#give").children(".cardBig");
    var giveCards = [];
    $.each(arrayElements, function() {
        giveCards.push($(this).attr("data-name"));
    });
    var arrayElements2 = $("#get").children(".cardBig");
    var getCards = [];
    $.each(arrayElements2, function() {
        getCards.push($(this).attr("data-name"));
    });

    giveCards.forEach(function(card) {
        CardManager.giveOwnCardToOtherPlayer(GameManager.getMainPlayer(), index, card);
    });
    getCards.forEach(function(card) {
        CardManager.giveOwnCardToOtherPlayer(index, GameManager.getMainPlayer(), card);
    });
    RemoteGameController.sendSyncCards();
    RemoteGameController.sendTrade({idTo: index, type: "confirm"});
    GameController.updateTable();
});

$(document).on("click", "#cancelTrade", function () {
    var index = $('#get').attr("data-id");
    RemoteGameController.sendTrade({idTo: index, type: "cancel"});
});

/*==Ask for help==*/

$(document).on("click", ".help", function () {
    var playerInfo = [];
    for (var i = 0; i < PlayerManager.getPlayersCount(); i++){
        if (i !== GameManager.getMainPlayer()){
        playerInfo.push({info:PlayerManager.getPlayerInfo(i), id:i});
        }
    }
    ViewManager.renderView({title: "help"}, 'modalWindows', 'askForHelp');
    ViewManager.renderView(playerInfo, 'playersDropDown', 'dropDown');
    $('.askHepl').modal('show');

});

$(document).on("click", ".player", function () {
    var selectPlayer = $(this).prop('class', 'select');
    var name = $(this).text();
    $('#namePlayer').text(name);
    $('#namePlayer').modal('show');
    $('.choosePlayer').modal('show');
    $('.giveCard').modal('show');
});

$(document).on("click", "#askHelp", function () {
    var reward = $('#numberCards').val();
    var id = $('.select').attr('data-id');
    RemoteGameController.sendAskForHelp({id: id, reward: reward, type: "ask"});
    $('.askHepl').modal('hide');
});

$(document).on("keyup", "#numberCards", function () {
    var reward = GameManager.calculateReward();
    var input = parseInt($("#numberCards").val());
    if (reward.treasures < input || input == NaN) {
        $("#askHelp").prop('disabled', true);
    } else {
        $("#askHelp").prop('disabled', false);
    }
});

$(document).on("click", ".confirmHelp", function () {
    ViewManager.confirmInformation("confirm", "Player asks for help. Will you help player?");
});

/*==End Help==*/

$(document).on("click", ".choice", function () {
    ViewManager.chooseCleance("choice", "Are you really want to do this?");
});

$(document).on("click", ".success", function () {
    ViewManager.callDialog("Look!", "You have extra 3 cards");
});


$(document).on("click", ".card", function () {
    var name = $(this).attr("data-name");
    var deckType = $(this).attr("data-deckType");
     ViewManager.renderView({data: {name : name, deckType: deckType}}, 'modalWindows', 'viewCard');
    $('.zoomCard').modal('show');
});

$(document).on("click", ".zoomCard", function () {
    $('.zoomCard').modal('hide');
});

/**
 * Clear View container
 */

$(document).on('hidden.bs.modal', '.modal', function () {
    ViewManager.cleanView('modalWindows');
});
/**
 * Voting finish
 */
$(document).on('finishVoting', function(e) {
    var currentPhase = GameManager.getPhase();
    GameManager.clearReadyPlayers();
    ViewManager.displayVoting([], GameManager.getMainPlayer());
    RemoteGameController.sendReadyState([]);
    if (currentPhase == "start") {
        GameController.phase("kickTheDoor");
    } else if (currentPhase == "combat") {
        GameController.phase("postCombat");
    }
});

/**
 * Confirm Ready state
 */
$(".ready").click(function () {
    var phase = GameManager.getPhase();

    if (phase == "start" || phase == "combat") {
        var playersCount = PlayerManager.getPlayersCount();
        var readyPlayers = GameManager.addReadyPlayer(GameManager.getMainPlayer());
        ViewManager.displayVoting(readyPlayers, GameManager.getMainPlayer());
        RemoteGameController.sendReadyState(readyPlayers);
        if (readyPlayers.length == playersCount) {
            var event = new Event('finishVoting');
            document.dispatchEvent(event);
        }
    } else if (phase == "finish") {
        var handStatus = GameController.checkHand();
        if (handStatus) {
            GameController.phase("nextTurn");
        } else {
            ViewManager.callDialog("Warning!", "You can't have more than 5 cards in hand.");
        }
    }
});

$(document).on("click", "#equipment > .cardBig", function () {
    $(".playMonster").prop('disabled', false);
    $("#equipment > .cardBig").removeClass("selectgiveCard");
    $(this).addClass("selectgiveCard");
});

/**
 * LF troubles choose
 */

$(document).on("click", "#door", function () {
    $("#monsters").hide();
});

$(document).on("click", "#fight", function () {
    $("#monsters").show();
});

$(document).on("click", "#monsters > .cardBig", function () {
    $("#monsters > .cardBig").removeClass("selectgiveCard");
    $(this).addClass("selectgiveCard");
});

$(document).on("click", ".submitLFTroubles", function () {
    var decision = $(".radioLFTraoubles:checked").attr("id");
    if (decision == "door") {
        var turnPlayerID = GameManager.getTurn();
        var playerCards = CardManager.getPlayerCards(turnPlayerID);
        CardManager.givePlayerDoor(turnPlayerID);
        ViewManager.cleanView("hand");
        ViewManager.renderView(playerCards.hand, "hand", "card");
        GameController.phase("finish");
    }
    if (decision == "fight") {
        var monster = $("#monsters").find(".selectgiveCard").attr("data-name");
        var card = CardManager.playHandCard(GameManager.getTurn(), monster);
        GameManager.addMonsterCard(card);
        GameController.updateTable();
        RemoteGameController.sendSyncCards();
        GameController.phase("combat");
    }
});

$(document).on("click", ".drawDoorCard", function () {
    var turnPlayerID = GameManager.getTurn();
    var playerCards = CardManager.getPlayerCards(turnPlayerID);
    CardManager.givePlayerDoor(turnPlayerID);
    ViewManager.cleanView("hand");
    ViewManager.renderView(playerCards.hand, "hand", "card");
    GameController.phase("finish");
});
/**
 * help interaction
 */
$(document).on("click", "#cancelHelp", function() {
    RemoteGameController.sendAskForHelp({type: "reject"});
});
$(document).on("click", "#confirmHelp", function() {
    var sidekickID = GameManager.getMainPlayer();
    var reward = GameManager.getSidekick().reward;
    GameManager.setSidekick({id: sidekickID, reward: reward});
    GameController.updateTable();
    RemoteGameController.sendAskForHelp({id: sidekickID, reward: reward, type: "agree"});
});

/**
 * Run Away
 */
$(document).on("click", ".runA", function() {
    GameController.runAway();
    GameController.phase("finish");
})