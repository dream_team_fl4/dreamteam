function StorageManager() {
    function setPlayerName(playerName) {
        sessionStorage.setItem('name', playerName);
    }

    function setPlayerGender (playerGender) {
        sessionStorage.setItem('gender', playerGender);
    }

    function setPlayerId (id) {
        sessionStorage.setItem('id', id);
    }

    function getPlayerName() {
        return sessionStorage.getItem('name');
    }

    function getPlayerGender() {
        return sessionStorage.getItem('gender');
    }

    function getPlayerId() {
        return sessionStorage.getItem('id');
    }

    function clearPlayerInfo() {
        sessionStorage.removeItem('name');
        sessionStorage.removeItem('gender');
        sessionStorage.removeItem('id');
    }

    return {
        setPlayerName: setPlayerName,
        setPlayerGender: setPlayerGender,
        setPlayerId: setPlayerId,
        getPlayerName: getPlayerName,
        getPlayerGender: getPlayerGender,
        getPlayerId: getPlayerId,
        clearPlayerInfo: clearPlayerInfo
    }
}
