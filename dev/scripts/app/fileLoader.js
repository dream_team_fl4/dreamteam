function FileLoader() {

  function loadDoorCards() {
    return _loadCards('door', 'cards/ready/doors');
  }

  function loadTreasureCards() {
    return _loadCards('treasure', 'cards/ready/treasures');
  }

  function _loadCards(deckType, rootUrl) {
    var promise = _loadCardNames(rootUrl + '/list.txt')
      .then(function (cardNames) {

        var promiseCards = [];
        var cards = [];

        for (var i = 0; i < cardNames.length; i++) {
          var url = rootUrl + '/' + cardNames[i];

          var promiseCard =_loadData(url).then(function (cardJSON) {
            var card = JSON.parse(cardJSON);

            card.deckType = deckType;
            cards.push(card);

            return card;
          });

          promiseCards.push(promiseCard);
        }

        return Promise.all(promiseCards).then(function (values) {
          return values;
        });

      }, function (error) {
        console.log(error);
      });

    return promise;
  }

  function _loadCardNames(url) {

    var promise = _loadData(url)
      .then(function (data) {
        var cardNames = data.split('\n');
        return cardNames;
      }, function (error) {
        console.log(error);
      });

    return promise;
  }

  function _loadData (url) {
    var promise = new Promise(function (resolve, reject) {
      function loadDoc() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
          if (xhttp.readyState == 4 && xhttp.status == 200) {
            resolve(xhttp.responseText);
          }
          else if (xhttp.status >= 400) {
            reject(xhttp.responseText);
          }
        };

        xhttp.open('GET', url, true);
        xhttp.send();
      }

      loadDoc();
    });

    return promise;
  }

  return {
    loadDoorCards: loadDoorCards,
    loadTreasureCards: loadTreasureCards
  }
}
