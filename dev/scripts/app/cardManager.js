function CardManager(playerCount) {
    var _doorCards = [];
    var _treasureCards = [];
    var _deskCards = [];

    var _doorRebound = [];
    var _treasureRebound = [];

    var _playersCards = [];

    var _fileLoader = new FileLoader();
    var _shuffler = new Shuffler();

    function getPlayerCards(playerIndex) {
        return _clone(_playersCards[playerIndex]);
    }

    function getDoorDeck() {
        return _clone(_doorCards);
    }

    function getTreasureDeck() {
        return _clone(_treasureCards);
    }

    function getDoorRebound() {
        return _clone(_doorRebound);
    }

    function getTreasureRebound() {
        return _clone(_treasureRebound);
    }

    function givePlayerDoor(playerIndex) {
        if (_doorCards.length === 0) {
            _doorCards = _doorRebound;
            _doorRebound = [];
            _shuffleDoors();
            _sendMessage('Door deck was shuffled');
        }

        var card = _doorCards.pop();
        var player = _playersCards[playerIndex];
        player.hand[card.name] = card;
        _sendMessage('{player} took door card');
    }

    function givePlayerTreasure(playerIndex, cardAmount) {

        if (typeof cardAmount === 'undefined') {
            cardAmount = 1;
        }

        var cards = [];

        for (var i = 0; i < cardAmount; i++) {

            if (_treasureCards.length === 0) {
                _treasureCards = _treasureRebound;
                _treasureRebound = [];
                _shuffleTreasures();
                _sendMessage('Treasure deck was shuffled');
            }

            var card = _treasureCards.pop();
            var player = _playersCards[playerIndex];

            player.hand[card.name] = card;
            cards.push(card);
            _sendMessage('{player} took treasure card');
        }

        return cards;
    }

    function givePlayerDoorFromRebound(playerIndex, cardName) {

        var player = _playersCards[playerIndex];

        for (var i = 0; i < _doorRebound.length; i++) {
            if (_doorRebound[i].name === cardName) {
                var card = _doorRebound.slice(i, 1)[0];

                player.hand[card.name] = card;
                break;
            }
        }
        _sendMessage('{player} took door card from rebound');
    }

    function givePlayerTreasureFromRebound(playerIndex, cardName) {

        var player = _playersCards[playerIndex];

        for (var i = 0; i < _treasureRebound.length; i++) {
            if (_treasureRebound[i].name === cardName) {
                var card = _treasureRebound.slice(i, 1)[0];

                player.hand[card.name] = card;
                break;
            }
        }
        _sendMessage('{player} took treasure card from rebound');
    }

    function giveOwnCardToOtherPlayer(playerIndexFrom, playerIndexTo, cardName) {
        var playerFrom = _playersCards[playerIndexFrom];
        var playerTo = _playersCards[playerIndexTo];

        var card;

        if (playerFrom.deskEnabled.hasOwnProperty(cardName)) {
            card = playerFrom.deskEnabled[cardName];
            delete playerFrom.deskEnabled[cardName];
        }
        else {
            card = playerFrom.hand[cardName];
            delete playerFrom.hand[cardName];
        }

        playerTo.hand[cardName] = card;
        _sendMessage('{player} gave {playerToApply} {card} for exchange', cardName, playerIndexTo);
    }

    function applyCurseCardToOtherPlayer(playerIndexFrom, playerIndexTo, cardName) {
        var playerFrom = _playersCards[playerIndexFrom];
        var playerTo = _playersCards[playerIndexTo];

        var card = playerFrom.hand[cardName];
        delete playerFrom.hand[cardName];

        playerTo.deskCurse[cardName] = card;
        _sendMessage('{player} applied curse {card} to {playerToApply}', cardName, playerIndexTo);
    }

    function applyCurseCardToCurrentPlayer(playerIndex, cardName) {
        debugger
        var player = _playersCards[playerIndex];

        var index;

        for (var i = 0; i < _deskCards.length; i++) {
            if (_deskCards[i].name === cardName) {
                index = i;
                break;
            }
        }

        var card = _deskCards.splice(index, 1)[0];
        player.deskCurse[cardName] = card;
        _sendMessage('{player} was cursed with {card}', cardName);
    }

    function playHandCard(playerIndex, cardName) {
        var playerCards = _playersCards[playerIndex];

        var card = playerCards.hand[cardName];
        delete playerCards.hand[cardName];

        _deskCards.push(card);
        _sendMessage('{player} put {card} on game desk', cardName);
        return card;
    }

    function playDeskCard(playerIndex, cardName) {
        var playerCards = _playersCards[playerIndex];

        var card = playerCards.deskEnabled[cardName];
        delete playerCards.deskEnabled[cardName];

        _deskCards.push(card);
        _sendMessage('{player} put {card} on game desk', cardName);
    }

    function playCurseOnDesk(playerIndex, cardName) {
        var playerCards = _playersCards[playerIndex];

        var card = playerCards.deskCurse[cardName];
        delete playerCards.deskCurse[cardName];

        _deskCards.push(card);
        _sendMessage('{player} put curse {card} on game desk', cardName);
    }

    function putEnabledCardOnPlayerDesk(playerIndex, cardName) {
        var playerCards = _playersCards[playerIndex];

        var card = playerCards.hand[cardName];
        delete playerCards.hand[cardName];

        playerCards.deskEnabled[cardName] = card;
        _sendMessage('{player} has equipped {card}', cardName);
    }

    function putDisabledCardOnPlayerDesk(playerIndex, cardName) {
        var playerCards = _playersCards[playerIndex];

        var card = playerCards.hand[cardName];
        delete playerCards.hand[cardName];

        playerCards.deskDisabled[cardName] = card;
        _sendMessage('{player} has equipped disabled {card}', cardName);
    }

    function takeCardFromDoorDeckAndPlay(playerIndex) {
        if (_doorCards.length === 0) {
            _doorCards = _doorRebound;
            _doorRebound = [];
            _shuffleDoors();
        }

        var card = _doorCards.pop();
        _sendMessage('{player} knocks the door');

        if (card.cardClass === 'monster') {
            _deskCards.push(card);
            _sendMessage('{player} fights against {card}', card.name);
        }
        else {
            var playerCards =_playersCards[playerIndex];
            playerCards.hand[card.name] = card;
            _sendMessage('{player} found {card} instead of monster ', card.name);
        }

        return card;
    }

    function putPlayerHandCardToRebound(playerIndex, cardName) {
        var playerCards = _playersCards[playerIndex];

        var card = playerCards.hand[cardName];
        delete playerCards.hand[cardName];

        _putCardToRebound(card);
        _sendMessage('{player} put to rebound {card}', cardName);
    }

    function putPlayerEnabledDeskCardToRebound(playerIndex, cardName) {
        var playerCards = _playersCards[playerIndex];

        var card = playerCards.deskEnabled[cardName];
        delete playerCards.deskEnabled[cardName];

        _putCardToRebound(card);
        _sendMessage('{player} put to rebound {card}', cardName);
    }

    function putPlayerDisabledDeskCardToRebound(playerIndex, cardName) {
        var playerCards = _playersCards[playerIndex];

        var card = playerCards.deskDisabled[cardName];
        delete playerCards.deskDisabled[cardName];

        _putCardToRebound(card);
        _sendMessage('{player} put to rebound {card}', cardName);
    }

    function putPlayerCurseDeskCardToRebound(playerIndex, cardName) {
        var playerCards = _playersCards[playerIndex];

        var card = playerCards.deskCurse[cardName];
        delete playerCards.deskCurse[cardName];

        _putCardToRebound(card);
        _sendMessage('{player} put to rebound curse {card}', cardName);
    }

    function putPlayerCardToRebound(playerIndex, cardName) {
        var playerCards = _playersCards[playerIndex];

        if (playerCards.hand[cardName]) {
            putPlayerHandCardToRebound(playerIndex, cardName);
        }
        else if (playerCards.deskEnabled[cardName]) {
            putPlayerEnabledDeskCardToRebound(playerIndex, cardName);
        }
        else if (playerCards.deskDisabled[cardName]) {
            putPlayerDisabledDeskCardToRebound(playerIndex, cardName);
        }
        else if (playerCards.deskCurse[cardName]) {
            putPlayerCurseDeskCardToRebound(playerIndex, cardName);
        }
        else {
            throw 'card is not found';
        }
    }

    function enableCardOnPlayerDesk(playerIndex, cardName) {
        var playerCards = _playersCards[playerIndex];

        var card = playerCards.deskDisabled[cardName];
        delete playerCards.deskDisabled[cardName];

        playerCards.deskEnabled[cardName] = card;
        _sendMessage('{player} enabled item {card}', cardName);
    }

    function replaceEnabledCardOnPlayerDesk(playerIndex, prevCardName, newCardName) {
        putPlayerEnabledDeskCardToRebound(playerIndex, prevCardName);
        putEnabledCardOnPlayerDesk(playerIndex, newCardName);
    }

    function putCardsToRebound() {
        var counter = 0;

        while (_deskCards.length > 0) {
            var card = _deskCards.pop();
            _putCardToRebound(card);
            counter++;
        }
    }

    function putNotUsedCardsBack(minNotConnectedPlayerIndex) {

        if (_playersCards.length === minNotConnectedPlayerIndex) return;

        for (var i = minNotConnectedPlayerIndex; i < 6; i++) {
            var playerCards = _playersCards[i].hand;
            for (var cardName in playerCards) {
                var card = playerCards[cardName];
                delete playerCards[cardName];

                if (card.deckType === 'door') {
                    _doorCards.push(card);
                }
                else {
                    _treasureCards.push(card);
                }
            }
        }

        _playersCards = _playersCards.splice(0, minNotConnectedPlayerIndex);
    }

    function init() {

        for (var i = 0; i < playerCount; i++) {
            _playersCards.push({
                hand: {},
                deskEnabled: {},
                deskDisabled: {},
                deskCurse: {}
            });
        }

        var promiseDoors = _fileLoader.loadDoorCards().then(function (data) {
            for (var i = 0; i < data.length; i++) {
                _doorCards.push(new Card(data[i]))
            }
        });

        var promiseTreasures = _fileLoader.loadTreasureCards().then(function (data) {
            for (var i = 0; i < data.length; i++) {
                _treasureCards.push(new Card(data[i]))
            }
        });

        return Promise.all([promiseDoors, promiseTreasures]).then(function () {
            _shuffleDoors();
            _shuffleTreasures();
            _shuffleTreasures();
        });
    }

    function _shuffleDoors() {
        _doorCards = _shuffler.shuffle(_doorCards);
    }

    function _shuffleTreasures() {
        _treasureCards = _shuffler.shuffle(_treasureCards);
    }

    function _clone(obj) {
        return JSON.parse(JSON.stringify(obj));
    }

    function _putCardToRebound(card) {
        if (card.deckType === 'door') {
            _doorRebound.push(card);
        }
        else if (card.deckType === 'treasure') {
            _treasureRebound.push(card);
        }
    }

    function getState() {
        return {
            doorCards: _clone(_doorCards),
            treasureCards: _clone(_treasureCards),
            deskCards: _clone(_deskCards),
            doorRebound: _clone(_doorRebound),
            treasureRebound: _clone(_treasureRebound),
            playersCards: _clone(_playersCards)
        }
    }

    function setState(state) {
        _doorCards = state.doorCards;
        _treasureCards = state.treasureCards;
        _deskCards = state.deskCards;
        _doorRebound = state.doorRebound;
        _treasureRebound = state.treasureRebound;
        _playersCards = state.playersCards;
    }

    function _sendMessage(message, cardName, playerToApply) {
        if (cardName) {
            message = message.replace('{card}', _normalizeCardName(cardName));
        }

        if (typeof(playerToApply) != 'undefined') {
            message = message.replace('{playerToApply}', PlayerManager.getPlayerInfo(playerToApply).name);
        }

        RemoteGameController.sendSystemMessage(message);
    }

    function _normalizeCardName(name) {
        return _toTitleCase(name.replace(/_/g, ' '));
    }

    function _toTitleCase(str) {
        return str.replace(/\w\S*/g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }


    return {
        // loading all card from JSON files
        init: init,
        // take a card from a door deck and give it to a player
        givePlayerDoor: givePlayerDoor,
        // take a card from a treasure deck and give it to a player.
        // givePlayerTreasure(playerIndex) or givePlayerTreasure(playerIndex, cardAmount)
        givePlayerTreasure: givePlayerTreasure,
        // find a card in a door rebound and give it to a player.
        givePlayerDoorFromRebound: givePlayerDoorFromRebound,
        // find a card in a treasure rebound and give it to a player.
        givePlayerTreasureFromRebound: givePlayerTreasureFromRebound,
        // take card from hand and play it on the desk
        playHandCard: playHandCard,
        // take one of enabled cards on the player desk and play it
        playDeskCard: playDeskCard,
        // take card-debuff and show on table
        playCurseOnDesk: playCurseOnDesk,
        // take a card from player hand and put it as enabled card on the player desk
        putEnabledCardOnPlayerDesk: putEnabledCardOnPlayerDesk,
        // take a card from player hand and put it as disabled card on the player desk
        putDisabledCardOnPlayerDesk: putDisabledCardOnPlayerDesk,
        // take a disabled card from the player desk and make it enabled
        enableCardOnPlayerDesk: enableCardOnPlayerDesk,
        // when players move is over, all card from the desk are moved to rebound
        putCardsToRebound: putCardsToRebound,
        // return player cards. those cards are cloned to prevent changes from outside of the CardManager
        getPlayerCards: getPlayerCards,
        // return all cards from door deck. those cards are cloned to prevent changes from outside of the CardManager
        getDoorDeck: getDoorDeck,
        // return all cards from treasure deck. those cards are cloned to prevent changes from outside of the CardManager
        getTreasureDeck: getTreasureDeck,
        // return cards from door rebound. those cards are cloned to prevent changes from outside of the CardManager
        getDoorRebound: getDoorRebound,
        // return cards from treasure rebound. those cards are cloned to prevent changes from outside of the CardManager
        getTreasureRebound: getTreasureRebound,
        // take an enabled card from a table of one player and give it to another player hand
        giveOwnCardToOtherPlayer: giveOwnCardToOtherPlayer,
        // take a curse card from a hand of one player and apply it to another player (put on his desk curse cards)
        applyCurseCardToOtherPlayer: applyCurseCardToOtherPlayer,
        // take a curse card from a desk and apply it to current player (put on his desk curse cards)
        applyCurseCardToCurrentPlayer: applyCurseCardToCurrentPlayer,
        // take a card from a door deck and put it on the desk. returns card.
        takeCardFromDoorDeckAndPlay: takeCardFromDoorDeckAndPlay,
        // take a card from a hand and place it to rebound
        putPlayerHandCardToRebound: putPlayerHandCardToRebound,
        // take a card from a enabled player items on the desk and place it to rebound
        putPlayerEnabledDeskCardToRebound: putPlayerEnabledDeskCardToRebound,
        // take a card from a disabled player items on the desk and place it to rebound
        putPlayerDisabledDeskCardToRebound: putPlayerDisabledDeskCardToRebound,
        // take a card from a curse player items on the desk and place it to rebound
        putPlayerCurseDeskCardToRebound: putPlayerCurseDeskCardToRebound,
        // take any player card and place it to rebound
        putPlayerCardToRebound: putPlayerCardToRebound,
        // take a card from a player hand and replace enabled item of the player desk.
        // the replaced card is gone to rebound
        replaceEnabledCardOnPlayerDesk: replaceEnabledCardOnPlayerDesk,
        // get all cards from decks and players and make a copy of them
        getState: getState,
        // set all cards of decks and players
        putNotUsedCardsBack: putNotUsedCardsBack,
        setState: setState
    }
}