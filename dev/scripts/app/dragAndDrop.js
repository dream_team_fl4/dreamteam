function DragAndDrop() {
    var dragObject = {};

    function onMouseDown(e) {

        if (e.which != 1) return;
        // if it's battle phase
        if (true) {
            var cardType = $(e.target).attr("data-drop");
            var cardName = $(e.target).attr("data-name");
            var elem;
            $(function(){
                elem = $(e.target).closest('.drag-' + cardType)[0];
            });
        }
        if (!elem) return;

        dragObject.elem = elem;
        dragObject.cardType = cardType;
        dragObject.cardName = cardName;

        dragObject.downX = e.pageX;
        dragObject.downY = e.pageY;

        return false;
    }

    function onMouseMove(e) {
        if (!dragObject.elem) return;

        if (!dragObject.avatar) {
            var moveX = e.pageX - dragObject.downX;
            var moveY = e.pageY - dragObject.downY;


            if (Math.abs(moveX) < 3 && Math.abs(moveY) < 3) {
                return;
            }

            dragObject.avatar = createAvatar(e);

            if (!dragObject.from) {
                dragObject.from = findDroppable(e);
            }

            if (!dragObject.avatar) {
                dragObject = {};
                return;
            }

            var coords = getCoords(dragObject.avatar);
            dragObject.shiftX = dragObject.downX - coords.left;
            dragObject.shiftY = dragObject.downY - coords.top;

            startDrag(e);
        }

        dragObject.avatar.style.left = e.pageX - dragObject.shiftX + 'px';
        dragObject.avatar.style.top = e.pageY - dragObject.shiftY + 'px';

        return false;
    }

    function onMouseUp(e) {
        if (dragObject.avatar) {
            finishDrag(e);
        }

        dragObject = {};
    }

    function finishDrag(e) {
        var dropElem = findDroppable(e);
        var isSame = dropElem == dragObject.from;

        if (!dropElem) {
            onDragCancel(dragObject);
        } else {
            onDragEnd(dragObject, dropElem, isSame);
        }
    }

    function createAvatar(e) {

        var avatar = dragObject.elem;
        var old = {
            parent: avatar.parentNode,
            nextSibling: avatar.nextSibling,
            position: avatar.position || '',
            left: avatar.left || '',
            top: avatar.top || '',
            zIndex: avatar.zIndex || ''
        };

        avatar.rollback = function() {
            old.parent.insertBefore(avatar, old.nextSibling);
            avatar.style.position = old.position;
            avatar.style.left = old.left;
            avatar.style.top = old.top;
            avatar.style.zIndex = old.zIndex
        };

        return avatar;
    }

    function startDrag(e) {
        var avatar = dragObject.avatar;

        $(".drop-" + dragObject.cardType).addClass("dropActive");

        document.body.appendChild(avatar);
        avatar.style.zIndex = 9999;
        avatar.style.position = 'absolute';
    }

    function findDroppable(event) {
        $(dragObject.avatar).hide();
        var elem = document.elementFromPoint(event.clientX, event.clientY);
        $(dragObject.avatar).show();

        if (elem == null) {
            return null;
        }
        var ret;
        $(function(){
            ret = $(elem).closest('.drop-' + dragObject.cardType)[0];
        });
        return ret;
    }

    document.onmousemove = onMouseMove;
    document.onmouseup = onMouseUp;
    document.onmousedown = onMouseDown;

    function onDragCancel(dragObject) {
        dragObject.avatar.rollback();
       $(function(){
           $(".drop-" + dragObject.cardType).removeClass("dropActive");
       });
    }

    function onDragEnd(dragObject, dropElem, isSame) {
        $(dragObject.elem).remove();
        $(dragObject.avatar).remove();

        if (isSame || !GameController.dragEnd(dragObject, $(dropElem) || isSame)) {
            dragObject.avatar.rollback();
        }
        /**
         * update table view
         */
        GameController.updateTable();
        $(function(){
            $(".drop-" + dragObject.cardType).removeClass("dropActive");
        });

    };

    function getCoords(elem) {
        var box = elem.getBoundingClientRect();

        return {
            top: box.top + pageYOffset,
            left: box.left + pageXOffset
        };

    }
}
