function RemoteGameController(connectionManager, cardManager, gameManager, playerManager, viewManager, chat, gameController) {
    function init() {
        // subscribe to server messages
        connectionManager.addMessageListener(function (data) {
            _processInitialCardsConfiguration(data);
            _processInitialPlayersConfiguration(data);
            _processTurnConfiguration(data);
            _processSyncCards(data);
            _processChatMessage(data);
            _processSystemMessage(data);
            _processJoinGame(data);
            _processStartGame(data);
            _processSyncPlayers(data);
            _processReadyState(data);
            _processBattlePowerUpdate(data);
            _processPhaseChange(data);
            _processLoseTurn(data);
            _processTrade(data);
            _processHelpRequest(data);
            _processLoseWindow(data);
        });

        var promise = connectionManager.init();
        return promise;
    }

    function sendChatMessage(message) {
        connectionManager.sendChatMessage(message);
    }

    function sendSystemMessage(message) {
        connectionManager.sendSystemMessage(message);
    }

    function sendSyncCards() {
        var state = cardManager.getState();

        state.playerCards = gameManager.getPlayerCards();
        state.monsterCards = gameManager.getMonsterCards();

        connectionManager.sendSyncCards(state);
    }

    function sendInitialCardsState() {
        connectionManager.sendInitialCardsState(cardManager.getState());
    }

    function sendJoinGame(data) {
        var state = data;
        connectionManager.sendJoinGame(state);
    }

    function sendSyncPlayers() {
        var state = playerManager.getPlayersState();
        connectionManager.sendSyncPlayers(state);
    }

    function sendInitialPlayersState() {
        var state = playerManager.getPlayersState();
        connectionManager.sendInitialPlayersState(state);
    }

    function sendTurnConfiguration() {
        var state = gameManager.syncTurn();
        connectionManager.sendTurnConfiguration(state);
    }

    function sendReadyState(state) {
        connectionManager.sendReadyState(state);
    }

    function sendBattlePower() {
        var state = {};
        state.playerPower = GameManager.getPlayerPower();
        state.monsterPower = GameManager.getMonsterPower();
        connectionManager.sendBattlePower(state);
    }

    function sendLoseTurn() {
        connectionManager.sendLoseTurn();
    }

    function sendPhaseChange(state) {
        connectionManager.sendPhaseChange(state);
    }

    function sendTrade(data) {
        connectionManager.sendTrade(data);
    }

    function sendAskForHelp(data) {
        connectionManager.sendAskForHelp(data);
    }

    function showLoseWindows() {
        connectionManager.showLoseWindows();
    }

    function _processInitialCardsConfiguration(data) {
        // if server sends initial door and treasure decks, save them in card manager
        if (data.type === 'initialCardsState') {
            cardManager.setState(data.message);
            gameController.updateTable();
        }
    }

    function _processSyncCards(data) {
        // sync cards between game clients
        if (data.type === 'syncCards') {
            cardManager.setState(data.message);
            gameManager.setCardsState(data.message);
            gameController.updateTable();
        }
    }

    function _processChatMessage(data) {
        // show chat message
        if (data.type === 'chat') {
            chat.addMessage(data.playerIndex, data.message);
        }
    }

    function _processSystemMessage(data) {
        // show chat message
        if (data.type === 'systemMessage') {
            chat.addSystemMessage(data.playerIndex, data.message);
        }
    }

    var isJoined = false;

    function _processJoinGame(data) {
        if (data.type === 'joinGame') {
            playerManager.updatePlayersState(data.message);

            if (!isJoined) {
                gameManager.setMainPlayer(data.playerIndex);

                if (data.playerIndex === 0) {
                    viewManager.showStartGameConfirmation(function () {
                        connectionManager.sendStartGame();
                        gameManager.rearrangeTurns();
                        cardManager.putNotUsedCardsBack(playerManager.getPlayersCount());
                        gameController.phase('start');
                    });
                }
                else {
                    viewManager.showGameWaitingMessage(playerManager.getPlayersCount());
                }

                isJoined = true;
            }

            if (data.isGameStarted) {
                _startGame();
            }

            sendSyncPlayers();
            gameController.updateTable();
        }
    }

    function _processStartGame(data) {
        if (data.type === 'startGame') {
            sendSystemMessage("Game started. Equip your items and press 'ready' button.");
            _startGame();
        }
    }

    function _startGame() {
        setTimeout(viewManager.unblockUI, 2000);

        gameManager.rearrangeTurns();
        cardManager.putNotUsedCardsBack(playerManager.getPlayersCount());

        gameManager.startGame();
        gameController.phase('start');
    }

    function _processInitialPlayersConfiguration(data) {
        // sync cards between game clients at the game start
        if (data.type === 'initialPlayersState') {
            playerManager.updatePlayersState(data.message);
            gameManager.setMainPlayer(data.playerIndex);

            gameController.updateTable();
        }
    }

    function _processTurnConfiguration(data) {
        // sync turns
        if (data.type === 'turnConfiguration') {
            gameManager.syncTurn(data.message);
            gameController.updateTable();
        }
    }

    function _processSyncPlayers(data) {
        if (data.type === 'syncPlayers') {
            playerManager.updatePlayersState(data.message);
            viewManager.updatePlayersCount(playerManager.getPlayersCount());
            gameController.updateTable();
        }
    }

    function _processReadyState(data) {
        if (data.type === 'readyState') {
            var ready = GameManager.syncReadyPlayers(data.message);
            ViewManager.displayVoting(ready, gameManager.getMainPlayer());
        }
    }

    function _processBattlePowerUpdate(data) {
        if (data.type === 'battlePower') {
            GameManager.setPlayerPower(data.message.playerPower);
            GameManager.setMonsterPower(data.message.monsterPower);
            GameController.updateBattlePower();
        }
    }

    function _processLoseTurn(data) {
        if (data.type === 'loseTurn') {
            GameManager.endTurn();
            GameController.updateTable();
        }
    }

    function _processLoseWindow(data) {
        if (data.type === 'lose') {
            ViewManager.renderView({title: "Lose a game"}, 'modalWindows', 'loseWindow');
            $('.loseAgame').modal('show');
        }
    }

    function _processPhaseChange(data) {
        if (data.type === 'changePhase') {
            GameController.syncPhases(data.message);
        }
    }

    function _processTrade(data) {
        if (data.type === 'trade') {
            if (data.message.type == "ask" && GameManager.getMainPlayer() == data.message.idTo) {
                var playerInfo = PlayerManager.getPlayerInfo(+data.message.idFrom);
                ViewManager.confirmTrade("Confirm", {give: data.message.get, get: data.message.give,
                msg: playerInfo.name + " wants to exchange ", ok: "confirmTrade", cancel: "cancelTrade", id: data.message.idFrom});
            } else if (data.message.type == "confirm" && GameManager.getMainPlayer() == data.message.idTo) {
                ViewManager.callDialog("Look!", "Player accepted trade");
            } else if (data.message.type == "cancel" && GameManager.getMainPlayer() == data.message.idTo) {
                ViewManager.callDialog("Look!", "Trade ain't accepted");
            }
        }
    }

    function _processHelpRequest(data) {
        if (data.type === 'help') {
            if (data.message.type == "ask" && GameManager.getMainPlayer() == data.message.id) {
                var playerInfo = PlayerManager.getPlayerInfo(+data.message.id);
                ViewManager.confirmInformation("Confirm", {msg: playerInfo.name + " needs your help for "+ data.message.reward +" treasures", ok: "confirmHelp", cancel: "cancelHelp"});
                GameManager.setSidekick({reward: parseInt(data.message.reward)});
            } else if (data.message.type == "reject" && GameManager.getMainPlayer() == GameManager.getTurn()) {
                ViewManager.callDialog("Warning!", "Your help request was rejected.");
            } else if (data.message.type == "agree") {
                if (GameManager.getMainPlayer() == GameManager.getTurn()) {
                    ViewManager.callDialog("Great!", "You have a sidekick now!");
                    RemoteGameController.sendSystemMessage(PlayerManager.getPlayerInfo(data.message.id).name + " now assisting " + PlayerManager.getPlayerInfo(GameManager.getTurn()).name);
                }
                GameManager.setSidekick({id: data.message.id, reward: data.message.reward});
            }
            GameController.updateTable();
        }
    }

    return {
        init: init,
        sendChatMessage: sendChatMessage,
        sendSystemMessage: sendSystemMessage,
        // send all cards state
        sendSyncCards: sendSyncCards,
        // send all cards state at the game start
        sendInitialCardsState: sendInitialCardsState,
        // add player in game
        sendJoinGame: sendJoinGame,
        // send all players info
        sendSyncPlayers: sendSyncPlayers,
        // send all players info at the game start
        sendInitialPlayersState: sendInitialPlayersState,
        // send turn configuration
        sendTurnConfiguration: sendTurnConfiguration,
        // send battle new power setted by host
        sendBattlePower: sendBattlePower,
        // sent info that player lost his turn
        sendLoseTurn: sendLoseTurn,
        // send phase change message
        sendPhaseChange: sendPhaseChange,
        // send ready players state
        sendReadyState: sendReadyState,
        // send trade communication message
        sendTrade: sendTrade,
        // send show lose window to players
        showLoseWindows: showLoseWindows,
        // send ask for help request
        sendAskForHelp: sendAskForHelp
    }
}