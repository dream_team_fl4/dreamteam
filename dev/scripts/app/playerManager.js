/** Class representing a Player's Manager. */
function PlayerManager() {
    var _players = [];

    /**
     * Get all players.
     * @return {array} - all players.
     */
    function getPlayersCount() {
        return _players.length;
    }

    /**
     * Set hero card for each player
     */
    function setHeroCards() {
        var randomOrder = getRandomArray(0, 7);
        _players.forEach(function(player, index) {
            player.setHero(randomOrder[index]);
        });
    }

    /**
     * Get player's lvl.
     * @param {number} playerIndex - target player's index.
     * @return {array} - all players.
     */
    function getPlayerLvl(index) {
        return _players[index].getLevel();
    }

    /**
     * Add player to game.
     * @param {object} config - player configuration object.
     */
    function addPlayer(config) {
        var newPlayer = new Player(config);
        _players.push(newPlayer);
    }

    /**
     * Reverse player's gender.
     * @param {number} playerIndex - target player's index.
     */
    function reverseGender(playerIndex) {
        if (typeof playerIndex == "number") {

            var currentGender = _players[playerIndex].getGender();
            currentGender = currentGender == "male" ? "female" : "male";
            _players[playerIndex].setGender(currentGender);
            _sendMessage('{player} changed gender to ' + currentGender);

        } else {
            throw "Error: can't reverse gender with wrong data";
        }
    }

    /**
     * Raise player's level.
     * @param {number} n - raise value.
     * @param {number} playerIndex - target player's index.
     */
    function raiseLevel(n, playerIndex) {
        if (typeof n == "number" && typeof playerIndex == "number") {

            var currentLevel = _players[playerIndex].getLevel();
            _players[playerIndex].setLevel(currentLevel + n);
            _sendMessage('{player} reached ' + _players[playerIndex].getLevel() +  ' level');
            if (currentLevel + n >= 10) {
                ViewManager.renderView({title: "Win a game"}, 'modalWindows', 'winWindow');
                $('.winAgame').modal('show');
                RemoteGameController.showLoseWindows();
            }

        } else {
            throw "Error: can't raise level with wrong data";
        }
    }

    /**
     * Decrease player's level.
     * @param {number} n - decrease value.
     * @param {number} playerIndex - target player's index.
     */
    function decreaseLevel(n, playerIndex) {
        if (typeof n == "number" && typeof playerIndex == "number") {

            var currentLevel = _players[playerIndex].getLevel();
            if (currentLevel - n >= 1) {
                _players[playerIndex].setLevel(currentLevel - n);
                _sendMessage(_players[playerIndex].getName() + ' decreased level to ' + _players[playerIndex].getLevel());
            } else {
                console.log("Can't decrease level to " + (currentLevel - n));
            }

        } else {
            throw "Error: can't decrease level with wrong data";
        }
    }

    /**
     * Set new player's power.
     * @param {number} n - new power value.
     * @param {number} playerIndex - target player's index.
     */
    function setPlayerPower(n, playerIndex) {
        if (typeof n == "number" && typeof playerIndex == "number") {

            _players[playerIndex].setPower(n);

        } else {
            throw "Error: can't set player power with wrong data";
        }
    }

    /**
     * Get full player's information.
     * @param {number} playerIndex - target player's index.
     * @return {object} Full player information
     */
    function getPlayerInfo(playerIndex) {
        if (typeof playerIndex == "number") {
            var playerInfo = {
                name: _players[playerIndex].getName(),
                gender: _players[playerIndex].getGender(),
                lvl: _players[playerIndex].getLevel(),
                power: _players[playerIndex].getPower(),
                hero: _players[playerIndex].getHero()
            };
        } else {
            throw "Error: can't get player info with wrong data";
        }

        return playerInfo;
    }

    /**
     * Check if player a host.
     * @return {boolean} - if player is host - true.
     * @param {number} playerIndex - target player's index.
     */
    function isHost(playerIndex) {
        return _players[playerIndex].isHost();
    }

    /**
     * Get player Hero card ID.
     * @param {number} playerIndex - player ID
     * @return {number} - hero ID.
     */
    function getHero(playerIndex) {
        return _players[playerIndex].getHero();
    }

    /**
     * Get player Hero card ID.
     * @param {number} playerIndex - player ID
     * @param {number} heroID - new hero ID
     */
    function setHero(playerIndex, heroID) {
        if (typeof heroID == "number") {
            _players[playerIndex].setHero(heroID);
        } else {
            throw "Error: can't set new hero with wrong data";
        }
    }

    /**
     * Get current players state
     */
    function getPlayersState() {
        var state = [];
        for (var i = 0; i < _players.length; i++) {
            state.push(getPlayerInfo(i));
        }
        return state;
    }
    /**
     * Update data of all players
     */
    function updatePlayersState(data) {

        if (data.length < _players.length) {
            _players = _players.slice(0, data.length);
        }
        else if (data.length > _players.length) {
            for (var i = 0; i < data.length - _players.length; i++) {
                _players.push(new Player({ isHost: false }));
            }
        }

        data.forEach(function(playerData, index) {
            _players[index].setName(playerData.name),
            _players[index].setGender(playerData.gender),
            _players[index].setLevel(playerData.lvl),
            _players[index].setPower(playerData.power),
            _players[index].setHero(playerData.hero)
        });
    }

    function _sendMessage(message) {
        RemoteGameController.sendSystemMessage(message);
    }

    return {
        getPlayersCount: getPlayersCount,
        setHeroCards: setHeroCards,
        getPlayerLvl: getPlayerLvl,
        addPlayer: addPlayer,
        reverseGender: reverseGender,
        raiseLevel: raiseLevel,
        decreaseLevel: decreaseLevel,
        setPlayerPower: setPlayerPower,
        getPlayerInfo: getPlayerInfo,
        isHost: isHost,
        getHero: getHero,
        setHero: setHero,
        getPlayersState: getPlayersState,
        updatePlayersState: updatePlayersState
    }
}