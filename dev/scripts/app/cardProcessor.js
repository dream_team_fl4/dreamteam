function CardProcessor() {
    /**
     * Lose items if unequip special item
     */
    function cleanEquipedItems(cards, item) {
        var itemsToUnequip = [];
        var msg;
        /**
         * Universal Translator Device and Royalty case
         */
        if (item == "universal_translator_device" || item == "royalty") {
            for (var key in cards) {
                if ((key == "universal_translator_device" && key != item) || (key == "royalty" && key != item)) { return; }
                if (key == "shelly") { continue; }

                if (cards[key].cardClass == "ally") {
                    itemsToUnequip.push(cards[key]);
                }
            }
            msg = "You discard card which allows you wear few allies. Choose ally to save, rest will be discarded."
        }

        /**
         * Supermunchkin case
         */
        if (item == "supermunchkin") {
            for (var key in cards) {
                if (cards[key].cardClass == "class" && key != "supermunchkin") {
                    itemsToUnequip.push(cards[key]);
                }
            }
            msg = "You discard card which allows you wear two classes. Choose class to save, another be discarded."
        }
        ViewManager.chooseEquipment({
            title: "Warning!",
            msg: msg,
            ok: "okDiscardSpecial",
            cancel: "cancelDiscardSpecial"
        });
        ViewManager.renderView(itemsToUnequip, "equipment", "cardBig");
        $(document).on("click", "#okDiscardSpecial", function() {
            var cards = $("#equipment > .cardBig");
            cards.each(function(n, el) {
                if (!$(el).hasClass("selectgiveCard")) {
                    CardManager.putPlayerEnabledDeskCardToRebound(GameManager.getMainPlayer(), $(el).attr("data-name"));
                }
            });
            CardManager.putPlayerCardToRebound(GameManager.getMainPlayer(), item);
            GameController.refreshPower(GameManager.getMainPlayer());
            RemoteGameController.sendSyncCards();
            GameController.updateTable();
            $('.chooseEquipment').remove();
        });

        $(document).on("click", "#cancelDiscardSpecial", function() {
            $('.chooseEquipment').remove();
        });
    }

    /**
     * Check slot stutus
     * @return {object} object of slot status
     */
    function checkSlot(cards, item) {
        var status = {
            itemsToSwap: [],
            allow: true
        }

        /**
         * Royalty and Universal Translator Device case
         */
        if (item.cardClass == "ally") {
            var unlimitedAlly = false;
            for (var card in cards) {
                if (card == "royalty" || card == "universal_translator_device") { unlimitedAlly = true; }
            }
        }
        /**
         * supermunchkin case
         */
        if (item.name == "supermunchkin") {
            return status;
        }
        /**
         * equip class case
         */
        if (item.cardClass == "class") {
            var classes = 0;
            var isSupermunchkin = false;
            for (var card in cards) {
                if (cards[card].cardClass == "class") {
                    classes++;
                }
                if (card == "supermunchkin") {
                    isSupermunchkin = true;
                }
            }
            if (classes < 3 && isSupermunchkin) {
                return status;
            }
        }

        /**
         * if item has class limitation
         */
        if (item.class) {
            status.allow = false;
            for (var card in cards) {
                if (card == item.class) {
                    status.allow = true;
                    break;
                }
            }
        }

        /**
         * if item didn't pass class check
         */
        if (!status.allow) { return  status; }

        if (item.cardClass == "one hand") {
            var equipedSlots = 0;
            for (var card in cards) {
                if (cards[card].cardClass == "itemBuff") { continue; }
                if (cards[card].cardClass == "one hand") {
                    equipedSlots++;
                    status.itemsToSwap.push(cards[card]);
                } else if (cards[card].cardClass == "two hands") {
                    status.itemsToSwap.push(cards[card]);
                    equipedSlots = 2;
                }
            }
            if (equipedSlots <= 1 && item.cardClass == "one hand") { status.itemsToSwap = [] }
        } else if (item.cardClass == "two hands") {
            for (var card in cards) {
                if (cards[card].cardClass == "itemBuff") { continue; }
                if (cards[card].cardClass == "one hand" || cards[card].cardClass == "two hands") {
                    status.itemsToSwap.push(cards[card]);
                }
            }
        } else {
            for (var card in cards) {
                if (cards[card].cardClass == "itemBuff") { continue; }
                if (cards[card].name == "supermunchkin") { continue; }
                if (cards[card].name == "thimble" || item.name == "thimble") { continue; }
                if (cards[card].name == "shelly" || item.name == "shelly") { continue; }
                if (unlimitedAlly) {
                    if (item.cardClass == "ally") { continue; }
                }
                if (cards[card].cardClass == item.cardClass) {
                    status.itemsToSwap.push(cards[card]);
                }
            }
        }
        return status;
    }

    /**
     * Check current card placed to assist monster
     * @return {object} object of slot status
     */
    function checkMonsterCards(monsterCards, card) {
        var status = {
            cardsToPlay: [],
            allow: false
        };

        /**
         * common case
         */
        if (monsterCards.length == 0 || card.cardClass != "monster" ) {
            status.allow = true;
            status.cardsToPlay.push(card);
        }

        /**
         * if card is undead or demon
         */
        if (card.type == "undead demon") {
            monsterCards.every(function(item) {
                if (card.type.indexOf(item.type) != -1) {
                    status.allow = true;
                    status.cardsToPlay.push(card);
                    return false;
                } else {
                    return true;
                }
            });
        } else if (card.type) {
            monsterCards.every(function(item) {
                if ((item.type && item.type.indexOf(card.type) != -1) || (item.name == "undead" && card.type == "undead")) {
                    status.allow = true;
                    status.cardsToPlay.push(card);
                    return false;
                } else {
                    return true;
                }
            });
        }

        if (card.cardClass == "monster" && status.allow == false) {
            var playerCards = CardManager.getPlayerCards(GameManager.getMainPlayer());
            var wanderingMonsterCard;
            for (var key in playerCards.hand) {
                if (playerCards.hand[key].name.indexOf("wandering_monster") != -1) {
                    ViewManager.confirmInformation("Warning!", {
                        msg: "Do you want to use Wondering Monster card to add this monster in combat?",
                        ok: "okWM",
                        cancel: "cancelWM"
                    });
                    status.allow = "WM";
                    wanderingMonsterCard = playerCards.hand[key];
                    break;
                }
            }

            $("#okWM").off().on("click", function() {
                GameManager.addMonsterCard(wanderingMonsterCard);
                CardManager.playHandCard(GameManager.getMainPlayer(), wanderingMonsterCard.name);
                GameManager.addMonsterCard(card);
                CardManager.playHandCard(GameManager.getMainPlayer(), card.name);
                RemoteGameController.sendSyncCards();
                GameController.updateTable();
            });
        }

        /**
         * Kitten case
         */
        if (card.name == "kitten") {
            monsterCards.forEach(function(item) {
                if (item.name == "gunter") {
                    status.allow = true;
                    status.cardsToPlay.push(card);
                }
            });
        }
        return status;
    }

    /**
     * Process lvl Up card
     * @return {boolean} true - play card, false - return card in hand
     */
    function processLvlUp(card, index) {
        var status = true;
        var playerCards = CardManager.getPlayerCards(GameManager.getMainPlayer());

        if (PlayerManager.getPlayerLvl(index) == 9) {
            ViewManager.callDialog("Warning!", "Player can't lvl UP this way rigth now! He's lvl 9!");
            status = false;
        } else {
            if (card.name == "win_an_epic_game") {
                if (PlayerManager.getPlayerLvl(GameManager.getMainPlayer()) == 9) {
                    ViewManager.callDialog("Warning!", "You cant steal a level and Win! FIGHT");
                    status = false;
                } else if (PlayerManager.getPlayerLvl(index) == 1) {
                    ViewManager.callDialog("Warning!", "You can't steal lvl if player only 1 lvl");
                    status = false;
                } else {
                    PlayerManager.decreaseLevel(1, +index);
                    PlayerManager.raiseLevel(1, GameManager.getMainPlayer());
                }
            } else if (card.name == "win_wizard_battle") {
                PlayerManager.raiseLevel(1, index);

                for (var key in playerCards.deskEnabled) {
                    if (key == "wizard") {
                        CardManager.givePlayerTreasure(GameManager.getMainPlayer(), 1);
                    }
                }
            } else {
                PlayerManager.raiseLevel(1, index);
            }
        }

        return status;
    }

    /**
     * Process curse card
     * @return {boolean} is curse applied
     */
    function applyCurse(card, index) {
        var mainPlayerID;
        if (index == "self") {
            mainPlayerID = GameManager.getTurn();
            index = GameManager.getTurn();
            _sendMessage(PlayerManager.getPlayerInfo(GameManager.getMainPlayer()).name + " was cursed by " + card.name);
        } else {
            mainPlayerID = GameManager.getMainPlayer();
        }
        var status = true;
        var playerCards = CardManager.getPlayerCards(index);
        var targetPlayerLvl = PlayerManager.getPlayerLvl(index);
        var actionPlayetLvl = PlayerManager.getPlayerLvl(GameManager.getMainPlayer());
        /**
         * Special Curses
         */
        if (card.name == "de-corpcinated") {
            status = true;
            var roll;
            var monster;
            var playerCards = CardManager.getPlayerCards(index);
            var rebound = CardManager.getDoorRebound();
            var RA = 0;
            for (var item in playerCards.deskEnabled) {
                if (playerCards.deskEnabled[item].RA) {
                    RA += playerCards.deskEnabled[item].RA;
                }
            }
            if (PlayerManager.getHero(index) == 5) {
                RA += 1;
            }
            for (var name in rebound) {
                if (rebound[name].cardClass == "monster") {
                    monster = rebound[name];
                    break;
                }
            }
            if (monster) {
                if (monster.RA) {
                    RA += monster.RA;
                }
                roll = GameManager.roll();
                if (roll + RA>= 5) {
                    ViewManager.callDialog("Warning!", "R:" + roll + " RA:" + RA + ": Run successfully from " + monster.name);
                } else {
                    ViewManager.callDialog("Warning!", "R:" + roll + " RA:" + RA + ": Bad stuff from " + monster.name);
                    CardProcessor.applyBadStuff(monster, index)
                }
            }
        } else if (card.name == "hobo") {
            var playerCards = CardManager.getPlayerCards(index);
            var cardsToSteal = [];
            var cardToStealPos;
            var leftPlayerID;
            var rigthPlayerID;
            var hoboPos;
            if (index - 1 < 0) {
                leftPlayerID = PlayerManager.getPlayersCount() - 1;
            } else {
                leftPlayerID = index - 1;
            }
            if (index + 1 == PlayerManager.getPlayersCount()) {
                rigthPlayerID = 0;
            } else {
                rigthPlayerID = index + 1;
            }
            for (var key in playerCards.hand) {
                cardsToSteal.push(playerCards.hand[key].name);
            }
            hoboPos = cardsToSteal.indexOf("hobo");
            if (hoboPos != -1) {
                cardsToSteal.splice(hoboPos, 1);
            }
            if (cardsToSteal.length != 0) {
                cardToStealPos = getRandomInt(0, cardsToSteal.length-1);
                CardManager.giveOwnCardToOtherPlayer(index, leftPlayerID, cardsToSteal[cardToStealPos]);
                cardsToSteal.splice(cardToStealPos, 1);
                if (cardsToSteal.length != 0) {
                    cardToStealPos = getRandomInt(0, cardsToSteal.length-1);
                    CardManager.giveOwnCardToOtherPlayer(index, rigthPlayerID, cardsToSteal[cardToStealPos]);
                }
            }
        } else if (card.name == "knife_storm") {
            var phase = GameManager.getPhase();
            var turn = GameManager.getTurn();
            if (phase == "combat" && turn == index) {
                GameManager.clearTable();
                CardManager.putCardsToRebound();
                GameController.updateTable();
                GameController.phase("finish");
            }
        } else if (card.name == "little_dude") {
            status = false;
            for (var name in playerCards.deskEnabled) {
                if (playerCards.deskEnabled[name].cardClass == "head") {
                    CardManager.putPlayerCardToRebound(index, name);
                    status = true;
                    break;
                }
            }
            if (status && targetPlayerLvl != 1) {
                var roll = GameManager.roll();
                var playerCards = CardManager.getPlayerCards(GameManager.getTurn());
                var RA = 0;
                for (var key in playerCards.deskEnabled) {
                    if (playerCards.deskEnabled[key].RA) {
                        RA += playerCards.deskEnabled[key].RA;
                    }
                }
                if (PlayerManager.getHero(GameManager.getTurn()) == 5) {
                    RA += 1;
                }
                if (roll + RA < 5) {
                    ViewManager.callDialog("Warning!", "You tried to run away from little_dude, but failed. -1 level.");
                    PlayerManager.decreaseLevel(1, index);
                } else {
                    ViewManager.callDialog("Warning!", "You ran away from little_dude successfully.");
                }
            }
        } else if (card.name == "shrunk") {
            var itemsSatatus = GameController.getEquipedItemsPower(index);
            var cardToLose = itemsSatatus.name;
            if (cardToLose) {
                CardManager.putPlayerCardToRebound(index, cardToLose);
            } else {
                status = false;
            }
        } else if (card.name == "wazoo") {
            var leftPlayerID;
            var leftPlayerOldHero;
            if (index - 1 < 0) {
                leftPlayerID = PlayerManager.getPlayersCount() - 1;
            } else {
                leftPlayerID = index - 1;
            }
            leftPlayerOldHero = PlayerManager.getHero(leftPlayerID);
            effectedPlayerOldHero = PlayerManager.getHero(index);
            PlayerManager.setHero(leftPlayerID, effectedPlayerOldHero);
            PlayerManager.setHero(index, leftPlayerOldHero);
            RemoteGameController.sendSyncPlayers();
        }
        /**
         * common case
         */
        if (card.lose) {
            if (typeof card.lose == "number") {
                var lose = card.lose;
                while (lose != 0) {
                    if (targetPlayerLvl - 1 > 0) {
                        status = true;
                        PlayerManager.decreaseLevel(1, index);
                        targetPlayerLvl = PlayerManager.getPlayerLvl(index);
                        lose--;
                    } else {
                        break;
                    }
                }
                if (lose == card.lose) { status = false; }
            } else if (card.lose == "gender") {
                PlayerManager.reverseGender(index);
            } else if (card.lose == "handCards") {
                for (var name in playerCards.hand) {
                    if (card.name == name) { continue; }
                    CardManager.putPlayerCardToRebound(index, name);
                }
            } else if (typeof card.lose == "string") {
                status = false;
                for (var name in playerCards.deskEnabled) {
                    if (playerCards.deskEnabled[name].cardClass == card.lose) {
                        CardManager.putPlayerCardToRebound(index, name);
                        status = true;
                        if (card.lose == "ally") {
                            break;
                        }
                    }
                }
            }
            /**
             * if card apply debuff if player haven't slot to lose
             */
            if (!status && typeof card.lose == "string") {
                if (typeof card.debuff == "number") {
                    if (targetPlayerLvl - card.debuff < 1 ) {
                        status = false;
                    } else {
                        PlayerManager.decreaseLevel(card.debuff, index);
                        status = true;
                    }
                }
            }
        }
        if (status && typeof card.debuff != "number" && card.debuff != undefined) {
            CardManager.applyCurseCardToOtherPlayer(mainPlayerID, index, card.name);
        } else if (status || mainPlayerID == index) {
            CardManager.putPlayerCardToRebound(mainPlayerID, card.name);
        } else {
            ViewManager.callDialog("Warning!", "Can't apply curse");
        }
        GameController.refreshPower(index);
        return status;
    }

    /**
     * Process curse card
     * @return {boolean} status of operation result
     */
    function applyBadStuff(card, index) {
        var playerCards = CardManager.getPlayerCards(index);
        var targetPlayerLvl = PlayerManager.getPlayerLvl(index);
        var cardCopy = JSON.parse(JSON.stringify(card));
        var status = true;
        /**
         * Special Monsters
         */
        if (card.debuff == "dead") {
            var sidekick = GameManager.getSidekick();
            GameController.lootTheBody(index, sidekick.id);
            return status;
        }

        if (cardCopy.name == "cuties") {
            var worth = 0;
            var items = [];
            var cardToDiscard;
            for (var key in playerCards.deskEnabled) {
                if (playerCards.deskEnabled[key].worth) {
                    items.push(playerCards.deskEnabled[key]);
                }
            }
            while (worth <   500) {
                cardToDiscard = findMin(items, "worth");
                items.splice(cardToDiscard.index, 1);
                worth += cardToDiscard.card.worth;
                if (worth == 4000) { break; }
                CardManager.putPlayerCardToRebound(index, cardToDiscard.card.name);
            }
        } else if (cardCopy.name == "giant") {
            var playerCards = CardManager.getPlayerCards(index);
            var cardsToSteal = [];
            var cardToStealPos;
            var leftPlayerID;
            var rigthPlayerID;
            if (index - 1 < 0) {
                leftPlayerID = PlayerManager.getPlayersCount() - 1;
            } else {
                leftPlayerID = index - 1;
            }
            if (index + 1 == PlayerManager.getPlayersCount()) {
                rigthPlayerID = 0;
            } else {
                rigthPlayerID = index + 1;
            }
            for (var key in playerCards.hand) {
                cardsToSteal.push(playerCards.hand[key].name);
            }
            for (var key in playerCards.deskEnabled) {
                cardsToSteal.push(playerCards.deskEnabled[key].name);
            }
            if (cardsToSteal.length != 0) {
                cardToStealPos = getRandomInt(0, cardsToSteal.length-1);
                CardManager.giveOwnCardToOtherPlayer(index, leftPlayerID, cardsToSteal[cardToStealPos]);
                cardsToSteal.splice(cardToStealPos, 1);
                if (cardsToSteal.length != 0) {
                    cardToStealPos = getRandomInt(0, cardsToSteal.length-1);
                    CardManager.giveOwnCardToOtherPlayer(index, rigthPlayerID, cardsToSteal[cardToStealPos]);
                }
            }
        } else if (cardCopy.name == "gunter") {
            var rebound = CardManager.getDoorRebound();
            var applied = 0;
            rebound.every(function(card) {
                if (card.cardClass == "monster") {
                    CardProcessor.applyBadStuff(card, index);
                    applied++ ;
                }
                if (applied == 3) {
                    return false;
                } else {
                    return true;
                }
            });
        } else if (cardCopy.name == "gut_grinder") {
            for (var name in playerCards.deskEnabled) {
                if (playerCards.deskEnabled[name].worth >= 400 && playerCards.deskEnabled[name].cardClass != "itemBuff" && playerCards.deskEnabled[name].cardClass != "ally") {
                    CardManager.putPlayerCardToRebound(index, name);
                }
            }
            for (var name in playerCards.deskDisabled) {
                if (playerCards.deskEnabled[name].worth >= 400 && playerCards.deskEnabled[name].cardClass != "itemBuff" && playerCards.deskEnabled[name].cardClass != "ally") {
                    CardManager.putPlayerCardToRebound(index, name);
                }
            }
        } else if (cardCopy.name == "ice_king") {
            var playerInfo = PlayerManager.getPlayerInfo(index);
            if (playerInfo.gender == "male") {
                if (targetPlayerLvl - 1 > 0) {
                    PlayerManager.decreaseLevel(1, index);
                }
            } else if (playerInfo.gender == "female") {
                CardManager.applyCurseCardToCurrentPlayer(index, cardCopy.name);
            }
        } else if (cardCopy.name == "flame_king") {
            for (var name in playerCards.deskEnabled) {
                if (playerCards.deskEnabled[name].cardClass != "itemBuff" && playerCards.deskEnabled[name].cardClass != "ally") {
                    CardManager.putPlayerCardToRebound(index, name);
                }
            }
            for (var name in playerCards.hand) {
                if (playerCards.hand[name].deckType == "treasure") {
                    CardManager.putPlayerCardToRebound(index, name);
                }
            }
        } else if (cardCopy.name == "king_worm") {
            for (var name in playerCards.hand) {
                CardManager.putPlayerCardToRebound(index, name);
            }
            PlayerManager.decreaseLevel(1, index);
        } else if (cardCopy.name == "kitten" || cardCopy.name == "shark_and_cat") {
            var loseCards = [];
            var randomIndex;
            var count = 0;
            for (var key in playerCards.hand) {
                loseCards.push(playerCards.hand[key].name);
            }
            for (var key in playerCards.deskEnabled) {
                loseCards.push(playerCards.deskEnabled[key].name);
            }
            while (count < 3 && loseCards.length != 0) {
                randomIndex = getRandomInt(0, loseCards.length - 1);
                CardManager.putPlayerCardToRebound(index, loseCards[randomIndex]);
                if (cardCopy.name == "shark_and_cat") { break; }
                loseCards.splice(randomIndex, 1);
                count++;
            }
        } else if (cardCopy.name == "maja") {
            var playersQueue = [];
            var loseCards = [];
            var randomIndex;
            for (var key in playerCards.hand) {
                loseCards.push(playerCards.hand[key].name);
            }
            for (var key in playerCards.deskEnabled) {
                loseCards.push(playerCards.deskEnabled[key].name);
            }
            for (var i = 0; i < PlayerManager.getPlayersCount(); i++) {
                if (i == index) { continue; }
                playersQueue.push(i);
            }
            playersQueue.forEach(function(playerID) {
                randomIndex = getRandomInt(0, loseCards.length - 1);
                CardManager.giveOwnCardToOtherPlayer(index, playerID, loseCards[randomIndex]);
                loseCards.splice(randomIndex, 1);
            });
        } else if (cardCopy.name == "the_earl_of_lemongrab") {
            var roll = GameManager.roll();
            var lose = Math.round(roll/2);
            ViewManager.callDialog("Warning!", "You rolled " + roll + " and lose " + lose + " levels.");
            while (lose != 0) {
                if (targetPlayerLvl - 1 > 0) {
                    PlayerManager.decreaseLevel(1, index);
                    targetPlayerLvl = PlayerManager.getPlayerLvl(index);
                    lose--;
                } else {
                    break;
                }
            }
        } else if (cardCopy.name == "tiffany") {
            var playerToSwap;
            if (index - 1 < 0) {
                playerToSwap = PlayerManager.getPlayersCount() - 1;
            } else {
                playerToSwap = index - 1;
            }
            var copyCards = JSON.parse(JSON.stringify(CardManager.getPlayerCards(playerToSwap)));
            for (var name in playerCards.hand) {
                CardManager.giveOwnCardToOtherPlayer(index, playerToSwap, name);
            }
            for (var name in copyCards.hand) {
                CardManager.giveOwnCardToOtherPlayer(playerToSwap, index, name);
            }
            GameController.updateTable();
        } else if (cardCopy.name == "hunson_abadeer") {
            if (targetPlayerLvl - 1 > 0) {
                PlayerManager.decreaseLevel(1, index);
            }
            var sidekick = GameManager.getSidekick();
            GameController.lootTheBody(index, sidekick.id);
        }
        /**
         * common case
         */
        if (cardCopy.lose) {
            if (typeof cardCopy.lose == "number") {
                while (cardCopy.lose != 0) {
                    if (targetPlayerLvl - 1 > 0) {
                        PlayerManager.decreaseLevel(1, index);
                        targetPlayerLvl = PlayerManager.getPlayerLvl(index);
                        cardCopy.lose--;
                    } else {
                        break;
                    }
                }
            } else if (cardCopy.lose == "allHands") {
                for (var name in playerCards.deskEnabled) {
                    if (playerCards.deskEnabled[name].cardClass == "one hand" || playerCards.deskEnabled[name].cardClass == "two hands") {
                        CardManager.putPlayerCardToRebound(index, name);
                    }
                }
            } else if (cardCopy.lose == "handItem") {
                for (var name in playerCards.deskEnabled) {
                    if (playerCards.deskEnabled[name].cardClass == "one hand" || playerCards.deskEnabled[name].cardClass == "two hands") {
                        CardManager.putPlayerCardToRebound(index, name);
                        break;
                    }
                }
            } else if (cardCopy.lose == "handCards") {
                for (var name in playerCards.hand) {
                    CardManager.putPlayerCardToRebound(index, name);
                }
            } else if (typeof cardCopy.lose == "string") {
                status = false;
                if (cardCopy.lose == "oneShot") {
                    for (var name in playerCards.hand) {
                        if ( playerCards.hand[name].cardClass == cardCopy.lose ) {
                            CardManager.putPlayerCardToRebound(index, name);
                            status = true;
                        }
                    }
                } else {
                    for (var name in playerCards.deskEnabled) {
                        if (playerCards.deskEnabled[name].cardClass == cardCopy.lose || name == cardCopy.lose) {
                            CardManager.putPlayerCardToRebound(index, name);
                            status = true;
                        }
                    }
                }
            }

            if (!status && typeof cardCopy.lose == "string") {
                if (typeof cardCopy.debuff == "number") {
                    while (cardCopy.debuff != 0) {
                        if (targetPlayerLvl - 1 > 0) {
                            PlayerManager.decreaseLevel(1, index);
                            targetPlayerLvl = PlayerManager.getPlayerLvl(index);
                            cardCopy.debuff--;
                        } else {
                            break;
                        }
                    }
                }
            }
        } else if (card.BSChoose) {
            if (card.name == "demon_cat") {
                var roll = GameManager.roll();
                card.firstBS += roll;
                card.secondBS += roll;
            }
            ViewManager.chooseBS({
                            msg1: card.firstBS,
                            msg2: card.secondBS,
                            cardName: card.name,
                            playerID: index
                        });
            $("#chooseFirstBS").off().on("click", function() {
                var monsterName = $(this).attr("data-cardName");
                var playerID = parseInt($(this).attr("data-playerID"));
                var playerCards = CardManager.getPlayerCards(playerID);
                if (monsterName == "crystal_guardian") {
                    var targetPlayerLvl = PlayerManager.getPlayerInfo(playerID).lvl
                    if (targetPlayerLvl - 1 > 0) {
                        PlayerManager.decreaseLevel(1, index);
                    }
                } else if (monsterName == "demon_cat") {
                    var msg = $("#msg1BS").text();
                    var roll = parseInt(msg[msg.length - 1]);
                    var loseItems = [];
                    var randomIndex;
                    for (var key in playerCards.deskEnabled) {
                        if (playerCards.deskEnabled[key].cardClass != "ally" && playerCards.deskEnabled[key].cardClass != "itemBuff") {
                            loseItems.push(key);
                        }
                    }
                    while (roll != 0 && loseItems.length != 0) {
                        randomIndex = getRandomInt(0, loseItems.length - 1);
                        CardManager.putPlayerCardToRebound(playerID, loseItems[randomIndex]);
                        loseItems.splice(randomIndex, 1);
                        roll--;
                    }
                } else if (monsterName == "door_lord") {
                    var items = [];
                    var loseItem;
                    for (var key in playerCards.deskEnabled) {
                        if (playerCards.deskEnabled[key].cardClass != "ally" && playerCards.deskEnabled[key].cardClass != "itemBuff") {
                            items.push(playerCards.deskEnabled[key]);
                        }
                    }
                    loseItem = findMax(items, "worth");
                    if (loseItem.name) {
                        CardManager.putPlayerCardToRebound(playerID, loseItem.name);
                    }
                }
                GameController.updateTable();
            });

            $("#chooseSecondBS").off().on("click", function() {
                var monsterName = $(this).attr("data-cardName");
                var playerID = parseInt($(this).attr("data-playerID"));
                var playerCards = CardManager.getPlayerCards(playerID);
                if (monsterName == "crystal_guardian") {
                    CardManager.applyCurseCardToCurrentPlayer(playerID, monsterName);
                } else if (monsterName == "demon_cat") {
                    var msg = $("#msg1BS").text();
                    var roll = parseInt(msg[msg.length - 1]);
                    var loseHandCards = [];
                    var randomIndex;
                    for (var key in playerCards.hand) {
                        if (playerCards.hand[key].cardClass != "ally" && playerCards.hand[key].cardClass != "itemBuff") {
                            loseHandCards.push(key);
                        }
                    }
                    while (roll != 0 && loseHandCards.length != 0) {
                        randomIndex = getRandomInt(0, loseHandCards.length - 1);
                        CardManager.putPlayerCardToRebound(playerID, loseHandCards[randomIndex]);
                        loseHandCards.splice(randomIndex, 1);
                        roll--;
                    }
                } else if (monsterName == "door_lord") {
                    var itemsSatatus = GameController.getEquipedItemsPower(playerID);
                    if (itemsSatatus.name) {
                        CardManager.putPlayerCardToRebound(playerID, itemsSatatus.name);
                    }
                }
                GameController.updateTable();
            });
        }

        if (card.debuff === true) {
            CardManager.applyCurseCardToCurrentPlayer(index, card.name);
        }

        GameController.refreshPower(index);
        return status;
    }

    function processCancel(curses, card) {
        var displayCards = [];
        for (var key in curses) {
            if (curses[key].cardClass == card.cancel) {
                displayCards.push({name: key, deckType: curses[key].deckType});
            }
        }
        ViewManager.chooseCleance("Cancel debuff", "Please, choose debuff to cancel", "confirmCleanse", "cancelCleanse", card.name);
        ViewManager.renderView(displayCards, "effect", "cardBig");
        $(document).on("click", ".cardBig", function() {
            $(".cardBig").removeClass("selectgiveCard");
            $(this).addClass("selectgiveCard");
        });

        $(document).on("click", "#confirmCleanse", function() {
            var cleanceCard = $(".chooseAright").attr("data-cleanceCard");
            var debuffName = $(".selectgiveCard").attr("data-name");
            CardManager.putPlayerCardToRebound(GameManager.getMainPlayer(), cleanceCard);
            if (debuffName) {
                _sendMessage(PlayerManager.getPlayerInfo(GameManager.getMainPlayer()).name + " canceled " + debuffName);
                CardManager.putPlayerCardToRebound(GameManager.getMainPlayer(), debuffName);
            }
            $('.chooseAright').remove();
            GameController.updateTable();
            RemoteGameController.sendSyncCards();
        });

        $(document).on("click", "#cancelCleanse", function() {
            $('.chooseAright').remove();
        });
    }

    function _sendMessage(message) {
        RemoteGameController.sendSystemMessage(message);
    }

    return {
        cleanEquipedItems: cleanEquipedItems,
        checkSlot: checkSlot,
        checkMonsterCards: checkMonsterCards,
        processLvlUp: processLvlUp,
        applyCurse: applyCurse,
        applyBadStuff: applyBadStuff,
        processCancel: processCancel
    }
}

var CardProcessor = new CardProcessor();