function WebsocketClient() {
    var _client;

    function send(obj) {
        _client.send(JSON.stringify(obj));
    }

    var _messageListeners = [];

    function addMessageListener(listener) {
        _messageListeners.push(listener);
    }

    function init() {

        var promise = new Promise(function (resolve, reject) {
            _client = new WebSocket('ws://localhost:8081', 'echo-protocol');

            _client.onopen = function () {
                console.log('Connected');
                resolve();
            };

            _client.onclose = function (event) {
                if (event.wasClean) {
                    console.log('Connection is closed');
                } else {
                    console.log('Connection is broken');
                }

                console.log('Code: ' + event.code + ' reason: ' + event.reason);
            };

            _client.onmessage = function (event) {
                for (var i = 0; i < _messageListeners.length; i++) {
                    _messageListeners[i](JSON.parse(event.data));
                }
            };

            _client.onerror = function (error) {
                console.log('Error: ' + error.message);
                reject(error.message);
            };
        });

        return promise;
    }

    return {
        // connect to the server
        init: init,
        // send message
        send: send,
        // subscribe to messages from server
        addMessageListener: addMessageListener
    }
}
