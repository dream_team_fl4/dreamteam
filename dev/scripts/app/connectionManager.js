function ConnectionManager(gameController) {
    var _client = new WebsocketClient();

    function init() {
        return _client.init();
    }

    function _send(message) {
        message.playerIndex = getCurrentPlayerIndex();
        _client.send(message);
    }

    function addMessageListener(listener) {
        _client.addMessageListener(listener);
    }

    function sendChatMessage(message) {
        if (!GameManager.isGameStarted()) return;

        _send({
            type: 'chat',
            message: message
        });
    }

    function sendSystemMessage(message) {
        if (!GameManager.isGameStarted()) return;

        _send({
            type: 'systemMessage',
            message: message
        });
    }

    function sendSyncCards(state) {
        _send({
            type: 'syncCards',
            message: state
        });
    }

    function sendInitialCardsState(state) {
        _send({
            type: 'initialCardsState',
            message: state
        });
    }

    function sendInitialPlayersState(state) {
        _send({
            type: 'initialPlayersState',
            message: state
        });
    }

    function sendTurnConfiguration(state) {
        _send({
            type: 'turnConfiguration',
            message: state
        });
    }

    function sendJoinGame(state) {
        _send({
            type: 'joinGame',
            message: state
        });
    }

    function sendStartGame() {
        _send({
            type: 'startGame'
        });
    }

    function sendSyncPlayers(state) {
        _send({
            type: 'syncPlayers',
            message: state
        });
    }

    function sendReadyState(state) {
        _send({
            type: 'readyState',
            message: state
        });
    }

    function sendBattlePower(state) {
        _send({
            type: 'battlePower',
            message: state
        });
    }

    function sendPhaseChange(state) {
        _send({
            type: 'changePhase',
            message: state
        });
    }

    function sendLoseTurn() {
        _send({
            type: 'loseTurn'
        });
    }

    function sendTrade(state) {
        _send({
            type: 'trade',
            message: state
        });
    }

    function sendAskForHelp(state) {
        _send({
            type: 'help',
            message: state
        });
    }

    function showLoseWindows() {
        _send({
            type: 'lose'
        });
    }

    function getCurrentPlayerIndex() {
        return gameController.getMainPlayer();
    }

    return {
        // connect to server
        init: init,
        // send chat message
        sendChatMessage: sendChatMessage,
        sendSystemMessage: sendSystemMessage,
        // send all cards state
        sendSyncCards: sendSyncCards,
        // send all cards state at the game start
        sendInitialCardsState: sendInitialCardsState,
        // send all players state at the game start
        sendInitialPlayersState: sendInitialPlayersState,
        // send turn configuration
        sendTurnConfiguration: sendTurnConfiguration,
        // send new player data
        sendJoinGame: sendJoinGame,
        sendStartGame: sendStartGame,
        // send all players state
        sendSyncPlayers: sendSyncPlayers,
        // send ready status
        sendReadyState: sendReadyState,
        // send battle new power set by host
        sendBattlePower: sendBattlePower,
        // sent phase change message
        sendPhaseChange: sendPhaseChange,
        // sent info that player lost his turn
        sendLoseTurn: sendLoseTurn,
        //  send trade communication info
        sendTrade: sendTrade,
        //  send ask for help request
        sendAskForHelp: sendAskForHelp,
        // subscribe to messages from the server
        addMessageListener: addMessageListener,
        // show lose window for player
        showLoseWindows: showLoseWindows,
        getCurrentPlayerIndex: getCurrentPlayerIndex
    }
}