/** Class representing a game Player. */
function Player(config) {
    /**
    * Create a player.
    * @param {string} config.name - Player's name.
    * @param {string} config.gender - Player's gender.
    */
    var _name = config.name;
    var _gender = config.gender;
    var _lvl = config.lvl || 1;
    var _power = 1;
    var _handAmount = 0;
    var _isHost = config.isHost;
    var _hero = "";

    /**
     * Get the player name.
     * @return {string} - Player name string.
     */
    function getName() {
        return _name;
    }

    /**
     * Get the player gender.
     * @return {string} - Player gender string.
     */
    function getGender() {
        return _gender;
    }

    /**
     * Get the player level value.
     * @return {number} - Player level value.
     */
    function getLevel() {
        return _lvl;
    }

    /**
     * Get the player power value.
     * @return {number} - Player power value.
     */
    function getPower() {
        return _power;
    }

    /**
     * Get the players hero card.
     * @return {number} - Player power value.
     */
    function getHero() {
        return _hero;
    }

    /**
     *Check if player a host.
     * @return {boolean} - if player is host - true.
     */
    function isHost() {
        return _isHost;
    }

    /**
     * Set gender for player.
     * @param {string} gender - New player's gender.
     */
    function setName(name) {
        if (typeof name == "string") {
            _name = name;
        } else {
            throw "Error: name must be a string.";
        }
    }

    /**
     * Set gender for player.
     * @param {string} gender - New player's gender.
     */
    function setGender(gender) {
        if (typeof gender == "string") {
            _gender = gender;
        } else {
            throw "Error: gender must be a string.";
        }
    }

    /**
     * Set level for player.
     * @param {number} n - New level value.
     */
    function setLevel(n) {
        if (typeof n == "number") {
            _lvl = n;
        } else {
            throw "Error: lvl must be a number.";
        }
    }

    /**
     * Set attack power for player.
     * @param {number} n - New attack power value.
     */
    function setPower(n) {
        if (typeof n == "number") {
            _power = n;
        } else {
            throw "Error: power must be a number.";
        }
    }

    /**
     * Set hero card for player.
     * @param {number} id - New hero card ID.
     */
    function setHero(id) {
        if (typeof id == "number") {
            _hero = id;
        } else {
            throw "Error: hero ID must be a number.";
        }
    }

    return {
        getName: getName,
        getGender: getGender,
        getLevel: getLevel,
        getPower: getPower,
        getHero: getHero,
        isHost: isHost,
        setName: setName,
        setGender: setGender,
        setLevel: setLevel,
        setPower: setPower,
        setHero: setHero
    }
}
