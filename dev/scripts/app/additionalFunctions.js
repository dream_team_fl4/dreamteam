/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Returns a array of random unique integer between min (inclusive) and max (exclusive)
 */
function getRandomArray(min, max) {
    var arr = [];
    var pool = [];
    var randomNumber;
    for (var i = 0; i < max; i++) {
        pool[i] = i;
    }
    while(pool.length > 0) {
        randomNumber = getRandomInt(0, pool.length-1);
        arr.push(pool[randomNumber]);
        pool.splice(randomNumber, 1);
    }
    return arr;
}

function findCard(cards, name) {
    for (var key in cards) {
        for (var card in cards[key]) {
            if (card == name) {
                return JSON.parse(JSON.stringify(cards[key][card]));
            }
        }
    }
}

function findMax(arr, param) {
    var max = {};
    max[param] = 0;
    arr.forEach(function(obj) {
        if (obj[param] > max[param]) {
            max = JSON.parse(JSON.stringify(obj));
        }
    });
    return max;
}

function findMin(arr, param) {
    var min = {
        card: {},
        index: -1
    };
    min.card[param] = 4000;
    arr.forEach(function(obj, index) {
        if (obj[param] < min.card[param]) {
            min.card = JSON.parse(JSON.stringify(obj));
            min.index = index;
        }
    });
    return min;
}