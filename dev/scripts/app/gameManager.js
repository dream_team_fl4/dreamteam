/** Class representing a Game Manager. */
function GameManager() {
    var _main = 0;
    var _phase = "";
    var _sidekick = {};
    var _isGameStarted = false;

    var _monsterPower = 0;
    var _playerPower = 0;
    var _playerCards = [];
    var _monsterCards = [];
    var _readyPlayers = [];

    var _turns = getRandomArray(0, PlayerManager.getPlayersCount());

    _setBody();
    /**
     * Get main player.
     * @return {number} - main player ID.
     */
    function getMainPlayer() {
        return _main;
    }

    /**
     * Set main player.
     * @param {number} n - player ID.
     */
    function setMainPlayer(n) {
        _main = n;
        _setBody();
    }

    function _setBody() {
        $('body').removeClass();
        $('body').addClass('player-' + _main);
    }

    /**
     * Get game phase.
     * @return {string} - phase name.
     */
    function getPhase() {
        return _phase;
    }

    /**
     * Set current phase.
     * @param {string} name - phase name.
     */
    function startPhase(name) {
        $("#phaseName").text(name);
        _phase = name;
    }

    /**
     * Get sidekick.
     * @return {number} - sidekick ID.
     */
    function getSidekick() {
        return _sidekick;
    }

    /**
     * Set sidekick.
     * @param {object} obj - sidekick's Information.
     */
    function setSidekick(obj) {
        _sidekick = obj;
    }

    /**
     * Get monster power value.
     * @return {object} - monster power value.
     */
    function getMonsterPower() {
        return _monsterPower;
    }

    /**
     * Get player power value.
     * @return {object} - player power value.
     */
    function getPlayerPower() {
        return _playerPower;
    }

    /**
     * Get player cards in battle.
     * @return {array} - player cards in battle.
     */
    function getPlayerCards() {
        return _playerCards;
    }

    /**
     * Get monster cards in battle.
     * @return {array} - monster cards in battle.
     */
    function getMonsterCards() {
        return _monsterCards;
    }

    /**
     * Set monster power value.
     * @param {number} n - New monster power value.
     */
    function setMonsterPower(n) {
        if (typeof n == "number") {
            _monsterPower = n;
        } else {
            throw "Error: power must be a number.";
        }
    }

    /**
     * Set player power value.
     * @param {number} n - New player power value.
     */
    function setPlayerPower(n) {
        if (typeof n == "number") {
            _playerPower = n;
        } else {
            throw "Error: power must be a number.";
        }
    }

    /**
     * Add card to player's cards in battle.
     * @param {object} card - card.
     */
    function addPlayerCard(card) {
        if (typeof card == "object") {
            _playerCards.push(card);
        } else {
            throw "Error: card must be an object.";
        }
    }


    /**
     * Add card to monster's cards in battle.
     * @param {object} card - card.
     */
    function addMonsterCard(card) {
        if (typeof card == "object") {
            _monsterCards.push(card);
        } else {
            throw "Error: card must be an object.";
        }
    }

    /**
     * Clear Table from all cards and reser Battle Power values
     */
    function clearTable() {
        _monsterPower = 0;
        _playerPower = 0;
        _playerCards = [];
        _monsterCards = [];
        _sidekick = {};
    }

    /**
     * Return value between 1 and 6
     */
    function roll() {
        return Math.floor(Math.random() * 6) + 1;
    }

    /**
     * end player's turn
     */
    function endTurn() {
        var finishTurn = _turns.splice(0, 1);
        _turns.push(finishTurn[0]);
    }

    /**
     * get who's turn now
     * @return {number} player ID
     */
    function getTurn() {
        return _turns[0];
    }
    function getTurns() {
        return _turns;
    }
    /**
     * set turns
     * @param {array} arr - array of turns initialization by host
     * @return {array} of turns
     */
    function syncTurn(arr) {
        if (arr) {
            _turns = arr;
        }
        return _turns;
    }

    /**
     * Add ready player.
     * @param {number} id - id.
     * @return {array} array of ready players
     */
    function addReadyPlayer(id) {
        if (typeof id == "number") {
            if (_readyPlayers.indexOf(id) != -1) {
                ViewManager.callDialog("Warning!", "You are 'ready' already.");
            } else {
                _readyPlayers.push(id);
            }
        } else {
            throw "Error: player ID must be a number.";
        }
        return _readyPlayers;
    }

    /**
     * Sync ready players
     * @return {array} array of ready players
     */
    function syncReadyPlayers(array) {
        if (array) {
            _readyPlayers = array.slice(0);
        }
        return _readyPlayers;
    }

    /**
     * Clear ready players.
     */
    function clearReadyPlayers() {
        _readyPlayers = [];
    }

    /**
     * calculate win reward
     */
    function calculateReward() {
        var reward = {
            lvl: 0,
            treasures: 0
        }
        for (var i = 0; i < _monsterCards.length; i++) {
            if (_monsterCards[i].lvl) {
                reward.lvl += _monsterCards[i].lvl;
            }
            if (_monsterCards[i].treasure) {
                reward.treasures += _monsterCards[i].treasure;
            }
        }
        return reward;
    }

    function setCardsState(state) {
        _playerCards = state.playerCards;
        _monsterCards = state.monsterCards;
    }

    function rearrangeTurns() {
        var tmp = [];
        for (var i = 0; i < _turns.length; i++) {
            if (_turns[i] < PlayerManager.getPlayersCount()) {
                tmp.push(_turns[i]);
            }
        }

        _turns = tmp;
    }

    function isGameStarted() {
        return _isGameStarted;
    }

    function startGame() {
        _isGameStarted = true;
    }

    return {
        startGame: startGame,
        isGameStarted: isGameStarted,
        getMainPlayer: getMainPlayer,
        setMainPlayer: setMainPlayer,
        getPhase: getPhase,
        startPhase: startPhase,
        getSidekick: getSidekick,
        setSidekick: setSidekick,
        getMonsterPower: getMonsterPower,
        getPlayerPower: getPlayerPower,
        getPlayerCards: getPlayerCards,
        getMonsterCards: getMonsterCards,
        setMonsterPower: setMonsterPower,
        setPlayerPower: setPlayerPower,
        addPlayerCard: addPlayerCard,
        addMonsterCard: addMonsterCard,
        clearTable: clearTable,
        roll: roll,
        endTurn: endTurn,
        getTurn: getTurn,
        syncTurn: syncTurn,
        rearrangeTurns: rearrangeTurns,
        addReadyPlayer: addReadyPlayer,
        syncReadyPlayers: syncReadyPlayers,
        clearReadyPlayers: clearReadyPlayers,
        calculateReward: calculateReward,
        setCardsState: setCardsState,
        getTurns: getTurns
    }
}
