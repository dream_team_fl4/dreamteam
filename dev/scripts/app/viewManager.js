function ViewManager() {
    _templates = {}

    /**
     * Returns HTML template code
     */
    function getTemplate(name) {
        return _templates[name];
    }

    /**
     * Render part of game view.
     * @param {object} or {array} data - object with data for displaying.
     * @param {string} viewName - name of section for rendering.
     * @param {string} template - name of template to use for rendering.
     */
    function renderView(data, viewName, template) {
        var HTMLtemplate = _templates[template];
        var render = Handlebars.compile(HTMLtemplate);

        if (Array.isArray(data)) {
            for (var i = 0; i < data.length; i++) {
                $("#" + viewName).append(render(data[i]));
            }
        } else if (typeof data == "object") {
            for (var card in data) {
                if (data.hasOwnProperty(card)) {
                    $("#" + viewName).append(render(data[card]));
                }
            }
        }
    }

    /**
     * Clean view from inner HTML code
     */
    function cleanView(viewName) {
        $("#" + viewName).html("");
    }

    /**
     * Clean table from pre-rendered inner HTML code
     */
    function cleanTableView() {
        var views = ["other", "mob", "player", "info", "items", "hand", "discard"];
        views.forEach(function(view) {
            $("#" + view).html("");
        });
    }

    /**
     * Сall informative modal windows
     */
    function callDialog(title, msg) {
        ViewManager.renderView({data: {title: title, msg: msg}}, 'modalWindows', 'popUp');
        $('.dialogModal').modal('show');
    }

    function confirmInformation(title, data) {
        ViewManager.renderView({data: {title: title, msg: data.msg, okID: data.ok, cancelID: data.cancel, id: data.id}}, 'modalWindows', 'confirmInf');
        $('.confirm').modal('show');
    }
    function confirmTrade(title, data) {
        ViewManager.renderView({data: {title: title, msg: data.msg, okID: data.ok, cancelID: data.cancel, id: data.id}}, 'modalWindows', 'confirmTrade');
        ViewManager.renderView(data.give, "give", "cardBig");
        ViewManager.renderView(data.get, "get", "cardBig");
        $('.confirmTrade').modal('show');
    }

    function chooseCleance(title, msg, okID, cancelID, cleanceCard) {
        ViewManager.renderView({data: {title: title, msg: msg, okID: okID, cancelID: cancelID, cleanceCard: cleanceCard}}, 'modalWindows', 'cleance');
        $('.chooseAright').modal('show');
    }

    function lfTroubleChoose(config) {
        ViewManager.renderView(
            {data:
                {
                    hasMonster: config.monsters.length
                }
            }, 'modalWindows', 'lftroubles');
        ViewManager.renderView(config.monsters, "monsters", "cardBig");
        $('.lftroubles').modal('show');
    }

    function chooseBS(config) {
        ViewManager.renderView(
            {data:
                {
                    msg1: config.msg1,
                    msg2: config.msg2,
                    cardName: config.cardName,
                    playerID: config.playerID
                }
            }, 'modalWindows', 'chooseBadStuff');
        $('.chooseBadStuff').modal('show');
    }

    function chooseEquipment(config) {
        ViewManager.renderView(
            {data:
                {
                    title: config.title,
                    msg: config.msg,
                    okChooseEquipment: config.ok,
                    cancelChooseEquipment: config.cancel,
                    cardToEquip: config.cardToEquip
                }
            }, 'modalWindows', 'chooseEquipment');
        $('.chooseEquipment').modal('show');
    }

    /**
     * Update player voting displaying
     * @param {array} ready - array of ready players
     * @param {number} mainID - main player ID
     */
    function displayVoting(ready, mainID) {
        if (ready.length == 0) {
            $(".isReady").css("background-image", "");
        } else {
            ready.forEach(function(id) {
                if (id == mainID) {
                    $(".user_data").find(".isReady").css("background-image", "url(../images/ready.png)");
                } else {
                    $("#player"+ id).closest(".userContainer").find(".isReady").css("background-image", "url(../images/ready.png)");
                }
            });
        }

    }

    /**
     * Update players view's color
     * @param {turn} ready - id of turn
     */
    function updateDisabled(turn) {
        var sidekick = GameManager.getSidekick();
        if (turn == GameManager.getMainPlayer() || GameManager.getMainPlayer() == sidekick.id) {
            $(".user").parent().removeClass("disabled");
        } else {
            if (!$(".user").parent().hasClass("disabled")) {
                $(".user").parent().addClass("disabled");
            }
        }

        $(".character").each(function(index, elem) {
            if (turn == $(elem).attr("data-index") || $(elem).attr("data-index") == sidekick.id) {
                $(elem).parent().removeClass("disabled");
            } else {
                if (!$(elem).parent().hasClass("disabled")) {
                    $(elem).parent().addClass("disabled");
                }
            }
        });
    }

    var blockCss = {
        width: '275px',
        cursor: 'default',
        color: 'white',
        border: 'none',
        backgroundColor: 'none'
    };

    var overlayCSS = {
        opacity: 0.9,
        cursor: 'default'
    };

    function showStartGameConfirmation(callback) {
        $("#modalWindows").load("scripts/templates/startGameConfirmation.html", function() {
            $("#start-game").click(function () {
                callback();
                unblockUI();
            });
            $.blockUI({ message: $('#startGameConfirmation'), css: blockCss, overlayCSS: overlayCSS });
        });
    }

    function showGameWaitingMessage(playersCount) {
        $("#modalWindows").load("scripts/templates/startGameWaitingMessage.html", function() {
            $.blockUI({ message: $('#startGameWaitingMessage'), css: blockCss, overlayCSS: overlayCSS });
            updatePlayersCount(playersCount)
        });
    }

    function unblockUI() {
        $.unblockUI();
    }

    function updatePlayersCount(number) {
        $('#player-count').html(number + ' connected players');
        if (number > 2) {
            $('#start-game').removeAttr('disabled');
        }
    }

    /**
     * Load and save all templates
     */
    function loadAll() {
        var cardPromise = this.requestTemplate("card");
        var cardBigPromise = this.requestTemplate("cardBig");
        var mainPlayerPromise = this.requestTemplate("mainPlayer");
        var otherPlayersPromise = this.requestTemplate("otherPlayers");
        var monsterPowerPromise = this.requestTemplate("monsterPower");
        var playerPowerPromise = this.requestTemplate("playerPower");
        var discardPromise = this.requestTemplate("discard");
        var playerInfoPromise = this.requestTemplate('playerInfo');
        var windowAllhandCardsPromise = this.requestTemplate('windowAllhandCards');
        var windowAllTableCardsPromise = this.requestTemplate('windowAllTableCards')
        var sellCardsPromise = this.requestTemplate('sellCards');
        var sellWindowPromise = this.requestTemplate('sell');
        var curseWindowPromise = this.requestTemplate('curseWindow');
        var popUpPromise = this.requestTemplate('popUp');
        var otherPlayersCursesPromise = this.requestTemplate('otherPlayersCurses');
        var tradeCardsPromise = this.requestTemplate('tradeCards');
        var bagsPromise = this.requestTemplate('bags');
        var dropDownPromise = this.requestTemplate('dropDown');
        var askForHeplPromise = this.requestTemplate('askForHelp');
        var confirmInformationPromise = this.requestTemplate('confirmInf');
        var cleancePromise = this.requestTemplate('cleance');
        var viewCardPromise = this.requestTemplate('viewCard');
        var cardsForSellPromise = this.requestTemplate('cardsForTrade');
        var chooseBadStuffPromise = this.requestTemplate('chooseBadStuff');
        var lftroublesPromise = this.requestTemplate('lftroubles');
        var chooseEquipmentPromise = this.requestTemplate('chooseEquipment');
        var confirmTradePromise = this.requestTemplate('confirmTrade');
        var winWindowPromise = this.requestTemplate('winWindow');
        var loseWindowPromise = this.requestTemplate('loseWindow');

        return cardPromise
                .then(function(data) {
                    _templates.card = data;
                    return mainPlayerPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.mainPlayer = data;
                    return otherPlayersPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.otherPlayers = data;
                    return monsterPowerPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.monsterPower = data;
                    return playerPowerPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.playerPower = data;
                    return discardPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.discard = data;
                    return playerInfoPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.playerInfo = data;
                    return windowAllhandCardsPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.windowAllhandCards = data;
                    return sellCardsPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.sellCards = data;
                    return sellWindowPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.sell = data;
                    return curseWindowPromise;
                }, function(error) {
                    throw error;
                })
                 .then(function(data) {
                    _templates.curseWindow = data;
                    return cardBigPromise;
                }, function(error) {
                    throw error;
                })
                 .then(function(data) {
                    _templates.cardBig = data;
                    return windowAllTableCardsPromise;
                }, function(error) {
                    throw error;
                })
                  .then(function(data) {
                    _templates.windowAllTableCards = data;
                    return popUpPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.popUp = data;
                    return otherPlayersCursesPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.otherPlayersCurses = data;
                    return tradeCardsPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.tradeCards = data;
                    return bagsPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.bags = data;
                    return dropDownPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.dropDown = data;
                    return askForHeplPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.askForHelp = data;
                    return confirmInformationPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.confirmInf = data;
                    return cleancePromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.cleance = data;
                    return viewCardPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.viewCard = data;
                    return cardsForSellPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.cardsForTrade = data;
                    return chooseBadStuffPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.chooseBadStuff = data;
                    return lftroublesPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.lftroubles = data;
                    return chooseEquipmentPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.chooseEquipment = data;
                    return confirmTradePromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.confirmTrade = data;
                    return winWindowPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.winWindow = data;
                    return loseWindowPromise;
                }, function(error) {
                    throw error;
                })
                .then(function(data) {
                    _templates.loseWindow = data;
                }, function(error) {
                    throw error;
                })
    }

    /**
     * Returns promise of get template request
     */
    function requestTemplate(name) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', "scripts/templates/"+ name +".html");
            xhr.onreadystatechange = function() {
                if (xhr.readyState != 4) return;
                if (xhr.status != 200) {
                    reject(Error(xhr.status + ': ' + xhr.statusText));
                    return;
                }
                resolve(xhr.responseText);
            }
            xhr.send();
        });
    }

    return {
        getTemplate: getTemplate,
        renderView: renderView,
        cleanView: cleanView,
        cleanTableView: cleanTableView,
        updateDisabled: updateDisabled,
        loadAll: loadAll,
        requestTemplate: requestTemplate,
        callDialog: callDialog,
        confirmInformation: confirmInformation,
        confirmTrade:confirmTrade,
        lfTroubleChoose: lfTroubleChoose,
        chooseBS: chooseBS,
        chooseCleance:chooseCleance,
        displayVoting: displayVoting,
        showStartGameConfirmation: showStartGameConfirmation,
        showGameWaitingMessage: showGameWaitingMessage,
        showGameWaitingMessage: showGameWaitingMessage,
        unblockUI: unblockUI,
        updatePlayersCount: updatePlayersCount,
        chooseEquipment: chooseEquipment
    }
}
