function Chat(connectionManager, playerManager) {

    _initCollapse();
    _initButton();
    _initScroll();

    function _initButton() {
        var messages = document.getElementById('messages');
        var button = document.getElementById('chat-button');
        var input = document.getElementById('chat-input');

        button.onclick = function () {
            addMessage(connectionManager.getCurrentPlayerIndex(), input.value);

            connectionManager.sendChatMessage(input.value);
            input.value = '';

            return false;
        };
    }

    function _initScroll() {
        $('.messages-container').mCustomScrollbar({
            axis:"y",
            theme:"dark-thin",
            autoExpandScrollbar:true,
            autoHideScrollbar: true,
            advanced:{
                autoExpandHorizontalScroll:true,
                updateOnContentResize: true
            }
        });
    }

    function _initCollapse() {
        var $chat = $('#chat');
        var $icon = $('#collapse .fa');
        $('#collapse').click(function () {
            if ($chat.hasClass('collapsed')) {
                $chat.removeClass('collapsed');

                $icon.removeClass('fa-angle-double-left');
                $icon.addClass('fa-angle-double-right');
            }
            else {
                $chat.addClass('collapsed');

                $icon.removeClass('fa-angle-double-right');
                $icon.addClass('fa-angle-double-left');}
        });
    }

    function addMessage(arg1, message) {

        var name;
        var playerIndex;

        if (typeof arg1 === 'string') {
            name = arg1;
        }
        else {
            playerIndex = arg1;
            name = playerManager.getPlayerInfo(playerIndex).name;
        }

        var list = document.getElementById('messages');

        var item = document.createElement("LI");
        item.className = 'message-item';
        if (name === 'System') {
            item.className += ' system-message';
        }
        
        item.innerHTML = '<span class="chat-player">' + name + '</span>: <span class="chat-message">' + message + '</span>';

        if (typeof playerIndex !== 'undefined') {
            item.className += ' player-item-' + playerIndex;
        }

        list.appendChild(item);
        toastr.info(message, name);

        $('.messages-container').mCustomScrollbar('scrollTo', 'bottom');
    }

    function addSystemMessage(playerIndex, message) {
        var name = playerManager.getPlayerInfo(playerIndex).name;
        message = message.replace('{player}', name);

        addMessage('System', message);
    }

    return {
        addMessage: addMessage,
        addSystemMessage: addSystemMessage
    };
}