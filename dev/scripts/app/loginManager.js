function LoginManager() {
    function _savePlayerInfo() {
        var name = document.getElementById('sign-up-name').value;
        var genders = document.getElementsByName('sex');
        var selectedGender;

        for(var i = 0; i < genders.length; i++) {
            if(genders[i].checked == true) {
                selectedGender = genders[i].value;
            }
        }

        var storageManager = new StorageManager();

        storageManager.setPlayerName(name);
        storageManager.setPlayerGender(selectedGender);
        storageManager.setPlayerId(_guid());

        window.open('/table.html', '_self');

        return false;
    }

    function _guid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }

    var button = document.getElementById('sign-up-button');
    button.onclick = _savePlayerInfo;
}

var loginManager = new LoginManager();