function Shuffler() {

    function shuffle(cards) {
        var clone = cards.splice(0);
        var result = [];

        while (clone.length > 0) {

            //get a card and push it to temporary array
            var index = _random(0, clone.length);
            var card = clone[index];

            result.push(card);
            clone.splice(index, 1); //remove card from original array
        }

        return result;
    }

    function _random(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }

    return {
        shuffle: shuffle
    }
}
