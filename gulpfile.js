var gulp = require('gulp');
var webserver = require('gulp-webserver');

gulp.task('scripts', function () {
  return gulp.src(['dev/scripts/**/*'])
    .pipe(gulp.dest('dist/scripts'));
});

gulp.task('styles', function () {
  return gulp.src(['dev/css/**/*.css'])
    .pipe(gulp.dest('dist/css'));
});

gulp.task('cards', function () {
  return gulp.src(['dev/cards/**/*'])
    .pipe(gulp.dest('dist/cards'));
});

gulp.task('images', function () {
  return gulp.src(['dev/images/**/*'])
    .pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', function () {
    return gulp.src(['dev/css/fonts/**/*'])
        .pipe(gulp.dest('dist/css/fonts'));
});

gulp.task('html', function () {
  return gulp.src(['dev/*.html'])
    .pipe(gulp.dest('dist'));
});

gulp.task('watch', function () {
  gulp.watch('dev/scripts/**/*', ['scripts']);
  gulp.watch('dev/css/**/*.css', ['styles']);
  gulp.watch('dev/**/*.html', ['html']);
});

gulp.task('serve', function () {
  gulp.src('dist')
    .pipe(webserver({
      root: 'dist',
      port: 8080,
      livereload: true,
      open: true,
      fallback: 'home_page.html'
    }));
});

gulp.task('serveAndriy', function () {
  gulp.src('dist')
    .pipe(webserver({
      root: 'dist',
      port: 8080,
      livereload: false,
      open: true,
      fallback: 'home_page.html'
    }));
});

gulp.task('build', ['scripts', 'styles', 'html', 'cards', 'images', 'fonts', 'watch', 'serve']);
gulp.task('buildA', ['scripts', 'styles', 'html', 'cards', 'images', 'fonts', 'watch', 'serveAndriy']);
