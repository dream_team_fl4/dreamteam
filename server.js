function WsServer() {
    var WebSocketServer = require('websocket').server;
    var http = require('http');

    var _connections = [];
    var _cardsState;
    var _initialPlayersState;
    var _joinedPlayers = [];
    var _turnConfiguration;
    var _isGameStarted = false;

    var server = http.createServer(function (request, response) {
        console.log((new Date()) + ' Received request for ' + request.url);
        response.writeHead(404);
        response.end();
    });

    server.listen(8081, function () {
        console.log((new Date()) + ' Server is listening on port 8081');
    });

    var wsServer = new WebSocketServer({
        httpServer: server,
        // You should not use autoAcceptConnections for production
        // applications, as it defeats all standard cross-origin protection
        // facilities built into the protocol and the browser.  You should
        // *always* verify the connection's origin and decide whether or not
        // to accept it.
        autoAcceptConnections: false
    });

    function originIsAllowed(origin) {
        // put logic here to detect whether the specified origin is allowed.
        return true;
    }

    wsServer.on('request', function (request) {
        if (!originIsAllowed(request.origin)) {
            // Make sure we only accept requests from an allowed origin
            request.reject();
            console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
            return;
        }

        var connection = request.accept('echo-protocol', request.origin);
        _connections.push(connection);

        console.log((new Date()) + ' Connection accepted.');

        connection.on('message', function (message) {
            if (message.type === 'utf8') {

                var connectionIndex = _connections.indexOf(connection);

                var obj = JSON.parse(message.utf8Data);
                var data = {
                    playerIndex: obj.playerIndex,
                    type: obj.type,
                    message: obj.message
                };

                console.log('message type:', data.type, '\tplayer index:', data.playerIndex);

                _processInitialCardsState(connectionIndex, data);
                _processSyncCardsState(connectionIndex, data);
                _processInitialPlayers(connectionIndex, data);
                _processTurnConfiguration(connectionIndex, data);
                _processStartGame(connectionIndex, data);
                _processSystemMessage(connectionIndex, data);
                _processJoinGame(connectionIndex, data);
                _processOtherMessages(connectionIndex, data);
            }
        });

        connection.on('close', function (reasonCode, description) {
            console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
        });
    });

    function _processInitialCardsState(connectionIndex, data) {
        if (data.type !== 'initialCardsState') return;

        if (typeof _cardsState === 'undefined') {
            _cardsState = data.message;
        }
        else {
            data.message = _cardsState;
            _connections[connectionIndex].sendUTF(JSON.stringify(data));
        }
    }

    function _processSyncCardsState(connectionIndex, data) {
        if (data.type !== 'syncCards') return;

        _cardsState = data.message;
        _sendAllExceptMe(connectionIndex, data);
    }

    function _processInitialPlayers(connectionIndex, data) {
        if (data.type !== 'initialPlayersState') return;

        if (typeof _initialPlayersState === 'undefined') {
            _initialPlayersState = data.message;
        }
        else {
            data.message = _getActualPlayers();
            _connections[connectionIndex].sendUTF(JSON.stringify(data));
        }
    }

    function _processTurnConfiguration(connectionIndex, data) {
        if (data.type !== 'turnConfiguration') return;

        if (typeof _turnConfiguration === 'undefined') {
            _turnConfiguration = data.message;
        }
        else {
            data.message = _turnConfiguration;
            _connections[connectionIndex].sendUTF(JSON.stringify(data));
        }
    }

    function _processStartGame(connectionIndex, data) {
        if (data.type !== 'startGame') return;
        _isGameStarted = true;

        _sendAll(data);
    }

    function _processSystemMessage(connectionIndex, data) {
        if (data.type !== 'systemMessage') return;

        _sendAll(data);
    }

    function _processJoinGame(connectionIndex, data) {
        if (data.type !== 'joinGame') return;

        data.playerIndex = -1;

        for (var i = 0; i < _joinedPlayers.length; i++) {
            if (_joinedPlayers[i].id === data.message.id) {
                data.playerIndex = i;
                break;
            }
        }

        if (data.playerIndex === -1) {
            data.playerIndex = _joinedPlayers.length;
            _joinedPlayers.push(data.message);

            console.log('Player has been joined: ', data.message.name);
        }

        if (_initialPlayersState) {

            for (var i = 0; i < _joinedPlayers.length; i++) {
                _initialPlayersState[i].name = _joinedPlayers[i].name;
                _initialPlayersState[i].gender = _joinedPlayers[i].gender;
                _initialPlayersState[i].id = _joinedPlayers[i].id;
            }
        }

        data.message = _getActualPlayers();
        data.isGameStarted = _isGameStarted;
        
        _connections[connectionIndex].sendUTF(JSON.stringify(data));
    }

    function _getActualPlayers() {
        return _initialPlayersState.slice(0, _joinedPlayers.length);
    }


    function _processOtherMessages(connectionIndex, data) {

        if (data.type === 'initialCardsState'
            || data.type === 'initialPlayersState'
            || data.type === 'joinGame'
            || data.type === 'syncCards'
            || data.type === 'startGame'
            || data.type === 'systemMessage') return;
        
        _sendAllExceptMe(connectionIndex, data);
    }

    function _sendAllExceptMe(connectionIndex, data) {
        for (var i = 0; i < _connections.length; i++) {
            if (i !== connectionIndex) {
                _connections[i].sendUTF(JSON.stringify(data));
            }
        }
    }

    function _sendAll(data) {
        for (var i = 0; i < _connections.length; i++) {
            _connections[i].sendUTF(JSON.stringify(data));
        }
    }
}

var wsServer = new WsServer();